#! /usr/bin/env nodejs
import * as http from "http";
import * as path from "path";

import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as handlebars from "express-handlebars";
import * as session from "express-session";
import * as logger from "morgan";
import * as serveFavicon from "serve-favicon";
import * as socket_io from "socket.io";
import * as sourceMapSupport from "source-map-support";

import { CommandStation } from "./model/central";
import { get_constants, SECRET } from "./model/constants";
import { Graph } from "./model/graph";
import { Hardware } from "./model/hardware";
import { Lights } from "./model/lights";
import { Occupancy } from "./model/occupancy";
import { Turnouts } from "./model/turnouts";
import { Actions } from "./routes/actions";
import * as logs from "./routes/logs";

const app = express();
const server = http.createServer(app);
const io = socket_io(server);

sourceMapSupport.install();

// Promise rejection control
process.on("unhandledRejection", (reason, p) => {
    console.log("Unhandled Rejection at: Promise", p, "reason:", reason);
    // application specific logging, throwing an error, or other logic here
});

const rootDir = path.join(__dirname, "..");
const hbsInstance = handlebars.create({
    defaultLayout : "main.hbs",
    extname : "hbs",
    layoutsDir: path.join(rootDir, "views", "layouts"),
});

// view engine setup
app.set("views", path.join(rootDir, "views"));
app.engine("hbs", hbsInstance.engine as any);
app.set("view engine", "hbs");

app.use(serveFavicon(path.join(rootDir, "public", "favicon.ico")));

/* For logging request to the server */
app.use(logger("combined"));

app.use(express.static(path.join(rootDir, "public")));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended : false,
}));

app.use(session({
    resave : true,
    saveUninitialized : true,
    secret : SECRET,
}));

async function launch() {
    const railroadConstants = await get_constants();
    const hardwareInstance = new Hardware(railroadConstants.hardware);
    await hardwareInstance.init();
    const turnoutsInstance = new Turnouts(
        railroadConstants, hardwareInstance.bus, hardwareInstance.i2cLock);
    await turnoutsInstance.init();
    const lightsInstance = new Lights(
        railroadConstants, hardwareInstance.bus, hardwareInstance.i2cLock);
    await lightsInstance.init();
    const occupancyInstance = new Occupancy(
        hardwareInstance.bus, hardwareInstance.i2cLock,
        railroadConstants.hardware.i2c);
    await occupancyInstance.init();
    const sprog = new CommandStation(hardwareInstance);
    sprog.start();
    setTimeout(
        () => {
            sprog.setMode(railroadConstants.hardware.sprog);
            setTimeout(() => sprog.power(true), 1000);
        },
        3000);
    const graph = new Graph(
        railroadConstants.railroad, railroadConstants.locomotives,
        sprog, occupancyInstance, turnoutsInstance, io);
    const actionsInstance = new Actions(
        railroadConstants, hardwareInstance, graph, lightsInstance, io);
    actionsInstance.register(app);
    logs.register(io);
    server.listen(3000, () => console.log("listening on *:3000"));
}

void launch();
