# DCCJS
DCCJS is a node application to control a DCC model railway:

* locomotives are controlled through an SPROG command station. 
* turnouts are controlled by servos interfaced with a PCA9685 based card.

## Installation 

On the PC you can do:

* npm install
* grunt install will create the dist folder that can be copied to the raspberry pi. 
* you need to perform an npm install inside dist on the raspberry pi

## Adaptation to your layout
There are a lot of things to adapt to your layout in the res folder. Some templates are given in doc.

