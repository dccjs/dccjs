const path = require("path");
const webpack = require("webpack");

module.exports = {
  entry: {
    dccjs: "./front/main.ts",
    logs: "./front/logs.ts"
  },
  mode: "production",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/
      }
    ]
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, "public/js")
  },
  performance: { hints: false },
  resolve: {
    alias: {
      "jqueryui": "jquery-ui-dist/jquery-ui.js"
    },
    extensions: [ ".tsx", ".ts", ".js" ]
  },
};
