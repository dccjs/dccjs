/** As ts-mockito does not provide a way to mock interfaces. We create
 * empty stubs that implement the interface and that can be correctly mocked
 * by the library.
 */

import { IncomingMessage } from "http";
import {Namespace, Server} from "socket.io";

export class SocketioServer implements Server {

    public engine: { ws: any;  generateId: (id: IncomingMessage) => string; };
    public nsps: { [namespace: string]: Namespace; };
    public sockets: Namespace;
    public json: Server;
    public volatile: Server;
    public local: Server;

    constructor() {
        this.volatile = this;
        this.json = this;
        this.local = this;
        this.engine = { ws: null, generateId: (x) => "" };
        this.nsps = {};
        this.sockets = (null as any);
    }

    public checkRequest(req: any, fn: (err: any, success: boolean) => void): void {
        throw new Error("Method not implemented.");
    }

    public serveClient(v?: any): any {
        throw new Error("Method not implemented.");
    }

    public path(v?: any): any {
        throw new Error("Method not implemented.");
    }

    public adapter(v?: any): any {
        throw new Error("Method not implemented.");
    }
    public origins(v?: any): any {
        throw new Error("Method not implemented.");
    }
    public attach(port: any, opts?: any): Server {
        throw new Error("Method not implemented.");
    }
    public listen(port: any, opts?: any): Server {
        throw new Error("Method not implemented.");
    }
    public bind(srv: any): Server {
        throw new Error("Method not implemented.");
    }
    public onconnection(socket: any): Server {
        throw new Error("Method not implemented.");
    }
    public of(nsp: string): Namespace {
        throw new Error("Method not implemented.");
    }
    public close(fn?: (() => void) | undefined): void {
        throw new Error("Method not implemented.");
    }

    public on(event: any, listener: any): any {
        throw new Error("Method not implemented.");
    }
    public to(room: string): Namespace {
        throw new Error("Method not implemented.");
    }
    public in(room: string): Namespace {
        throw new Error("Method not implemented.");
    }
    public use(fn: (socket: import("socket.io").Socket, fn: (err?: any) => void) => void): Namespace {
        throw new Error("Method not implemented.");
    }
    public emit(event: string, ...args: any[]): Namespace {
        throw new Error("Method not implemented.");
    }
    public send(...args: any[]): Namespace {
        throw new Error("Method not implemented.");
    }
    public write(...args: any[]): Namespace {
        throw new Error("Method not implemented.");
    }
    public clients(...args: any[]): Namespace {
        throw new Error("Method not implemented.");
    }
    public compress(...args: any[]): Namespace {
        throw new Error("Method not implemented.");
    }
}
