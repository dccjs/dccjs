import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import { readdirSync } from "fs";
import "mocha";
import { dirname, join } from "path";

import { get_railroad } from "../../model/constants";

chai.use(chaiAsPromised);

const basePath = join(dirname(__dirname), "resources");

describe("load railroad", () => {
    describe("Good scenarios", () => {
        const goodPath = join(basePath, "good");
        const filenames = readdirSync(goodPath);
        for (const file of filenames) {
            it(file, async () => {
                const testFile = join(goodPath, file);
                const railroad = await get_railroad(testFile);
                chai.expect(railroad).not.to.equal(null);
            });
        }
    });
    describe("Bad scenarios", () => {
        const badPath = join(basePath, "bad");
        const filenames = readdirSync(badPath);
        for (const file of filenames) {
            it(file, async () => {
                const testFile = join(badPath, file);
                await chai.assert.isRejected(get_railroad(testFile));
            });
        }
    });

});
