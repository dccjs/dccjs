import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import "mocha";
import { dirname, join } from "path";

import { instance, mock } from "ts-mockito";

import { CommandStation } from "../../model/central";
import { get_locomotives, get_railroad } from "../../model/constants";
import { Graph } from "../../model/graph";
import { Occupancy } from "../../model/occupancy";
import { Turnouts } from "../../model/turnouts";
import { TurnoutState } from "../../model/types";
import { ILocomotiveImplem, IOccupancyImplem, ITurnoutImplem } from "../../model/types_implems";

import { SocketioServer } from "./mocks";

chai.use(chaiAsPromised);

const basePath = join(dirname(__dirname), "resources");

async function setScene(file: string): Promise<[Graph, IOccupancyImplem[], ILocomotiveImplem, ITurnoutImplem[]]> {
    const testFile = join(basePath, file);
    const locoDir = join(basePath, "locomotives");
    const railroad = await get_railroad(testFile);
    const locos = await get_locomotives(locoDir);
    const commandStation = mock (CommandStation);
    const occupancy = mock (Occupancy);
    const turnouts = mock(Turnouts);
    const io = mock(SocketioServer);
    const graph = new Graph(
        railroad, locos,
        instance(commandStation), instance(occupancy),
        instance(turnouts), io);
    return [graph, railroad.occupancy, locos[0], railroad.turnouts];
}

function checkOcc(vOcc: IOccupancyImplem[], vNames: Array<ILocomotiveImplem|null>, position: string) {
    function nameOf(loco: ILocomotiveImplem | null): string | null {
        return loco === null ? null : loco.name;
    }
    const found = vOcc.map((occ) => nameOf(occ.booked));
    chai.expect(found, `occupancies at ${position}`).to.eql(vNames.map(nameOf));
}

function checkLoco(
    loco: ILocomotiveImplem,
    where: IOccupancyImplem | null,
    next: IOccupancyImplem | null,
    was: IOccupancyImplem | null,
) {
    function nameOf(occ: IOccupancyImplem | null): string | null {
        return occ === null ? null : occ.name;
    }
    chai.expect(nameOf(loco.where), "where value").to.equal(nameOf(where));
    chai.expect(nameOf(loco.next), "next value").to.equal(nameOf(next));
    chai.expect(nameOf(loco.was), "was value").to.equal(nameOf(was));
}

const file1 = "circle.json";

describe("Base graph control", () => {
    it("Set occupancy", async () => {
        const [graph, occupancies] = await setScene(file1);
        const occ = occupancies[0];
        chai.expect(occ.state).to.equal(false);
        graph.onOccupancyChanged(occ.pin, true);
        chai.expect(occ.state).to.equal(true);
        graph.onOccupancyChanged(occ.pin, false);
        chai.expect(occ.state).to.equal(false);
    });
    it("Assign occupancy", async () => {
        const [graph, occupancies, loco] = await setScene(file1);
        const occ = occupancies[0];
        graph.setNext(loco, occ);
        chai.expect(occ.booked).to.equal(null);
        chai.expect(loco.where).to.equal(null);
        graph.onOccupancyChanged(occ.pin, true);
        graph.setNext(loco, occ);
        chai.expect(occ.booked).to.equal(loco);
        chai.expect(loco.where).to.equal(occ);
    });
    it("What is next ?", async () => {
        const [graph, occupancies, loco] = await setScene(file1);
        const [occ0, occ1, occ2, occ3] = occupancies;
        graph.onOccupancyChanged(occ0.pin, true);
        graph.setNext(loco, occ0);
        checkOcc(occupancies, [loco, loco, null, null], "non moving locomotive");
        checkLoco(loco, occ0, occ1, null);
    });
    it("What is next (inverted case) ?", async () => {
        const [graph, occupancies, loco] = await setScene(file1);
        const [occ0, occ1, occ2, occ3] = occupancies;
        graph.onOccupancyChanged(occ0.pin, true);
        graph.invert(loco);
        graph.setNext(loco, occ0);
        checkOcc(occupancies, [loco, null, null, loco], "non moving inverted locomotive");
        checkLoco(loco, occ0, occ3, null);
    });
    it("Invert speed no past.", async () => {
        const [graph, occupancies, loco] = await setScene(file1);
        const [occ0, occ1, occ2, occ3] = occupancies;
        graph.onOccupancyChanged(occ0.pin, true);
        graph.setNext(loco, occ0);
        graph.setSpeed(loco, 10, false);
        checkOcc(occupancies, [loco, null, null, loco], "moving backward locomotive");
        checkLoco(loco, occ0, occ3, null);
        graph.setSpeed(loco, 10, true);
        checkOcc(occupancies, [loco, loco, null, null], "moving forward again locomotive");
        checkLoco(loco, occ0, occ1, null);
    });
    it("Let's move !", async () => {
        const [graph, occupancies, loco] = await setScene(file1);
        const [occ0, occ1, occ2, occ3] = occupancies;
        graph.onOccupancyChanged(occ0.pin, true);
        graph.setNext(loco, occ0);
        graph.onOccupancyChanged(occ1.pin, true);
        checkLoco(loco, occ1, occ2, occ0);
        checkOcc(occupancies, [loco, loco, loco, null], "changing occ locomotive (forward)");
        graph.onOccupancyChanged(occ0.pin, false);
        graph.onOccupancyChanged(occ2.pin, true);
        checkLoco(loco, occ2, occ3, occ1);
        checkOcc(occupancies, [null, loco, loco, loco],  "changing occ locomotive (forward, step 2)");
    });

    it("Move with transient restore of previous", async () => {
        const [graph, occupancies, loco] = await setScene(file1);
        const [occ0, occ1, occ2, occ3] = occupancies;
        graph.onOccupancyChanged(occ0.pin, true);
        graph.setNext(loco, occ0);
        graph.onOccupancyChanged(occ1.pin, true);
        graph.onOccupancyChanged(occ0.pin, false);
        // This can occur as a transient state. power detected on previous
        // and turned off on current because of dirt/bad contact.
        graph.onOccupancyChanged(occ0.pin, true);
        graph.onOccupancyChanged(occ0.pin, false);
        checkLoco(loco, occ1, occ2, occ0);
        checkOcc(
            occupancies, [loco, loco, loco, null],
            "changing occ locomotive (forward after transient no current)");
    });

    it("Let's move back (quick) !", async () => {
        const [graph, occupancies, loco] = await setScene(file1);
        const [occ0, occ1, occ2, occ3] = occupancies;
        graph.onOccupancyChanged(occ0.pin, true);
        graph.setNext(loco, occ0);
        graph.onOccupancyChanged(occ1.pin, true);
        graph.onOccupancyChanged(occ0.pin, false);
        checkLoco(loco, occ1, occ2, occ0);
        checkOcc(occupancies, [loco, loco, loco, null], "moving forward");
        graph.onOccupancyChanged(occ2.pin, true);
        checkLoco(loco, occ2, occ3, occ1);
        checkOcc(occupancies, [null, loco, loco, loco], "moving forward 2");
        // Speed inversion while still on block
        graph.setSpeed(loco, 10, false);
        checkOcc(occupancies, [loco, loco, loco, null], "moving backward");
        checkLoco(loco, occ1, occ0, occ2);
    });
    it("Let's move back (slow) !", async () => {
        const [graph, occupancies, loco] = await setScene(file1);
        const [occ0, occ1, occ2, occ3] = occupancies;
        graph.onOccupancyChanged(occ0.pin, true);
        graph.setNext(loco, occ0);
        graph.onOccupancyChanged(occ1.pin, true);
        graph.onOccupancyChanged(occ0.pin, false);
        graph.onOccupancyChanged(occ2.pin, true);
        // Speed inversion after previous was released
        graph.onOccupancyChanged(occ1.pin, false);
        graph.setSpeed(loco, 10, false);
        checkOcc(occupancies, [null, loco, loco, null], "moving backward slow");
        checkLoco(loco, occ2, occ1, null);
    });
});

const file2 = "turnout.json";

describe("Paths and turnouts", () => {
    it("Change turnout 1", async () => {
        const [graph, occupancies, loco] = await setScene(file2);
        const [occ1, occ2, occ3] = occupancies;
        graph.turn(0, TurnoutState.Straight);
        graph.onOccupancyChanged(occ1.pin, true);
        graph.setNext(loco, occ1);
        graph.setSpeed(loco, 10, true);
        checkOcc(occupancies, [loco, loco, null], "turnout straight");
        checkLoco(loco, occ1, occ2, null);
        graph.turn(0, TurnoutState.Turn);
        checkOcc(occupancies, [loco, null, loco], "turnout turned");
        checkLoco(loco, occ1, occ3, null);
    });
    it("Change turnout 2", async () => {
        const [graph, occupancies, loco] = await setScene(file2);
        const [occ1, occ2, occ3] = occupancies;
        graph.turn(0, TurnoutState.Straight);
        graph.onOccupancyChanged(occ2.pin, true);
        graph.setNext(loco, occ2);
        graph.setSpeed(loco, 10, false);
        checkOcc(occupancies, [loco, loco, null], "turnout straigh");
        checkLoco(loco, occ2, occ1, null);
        graph.turn(0, TurnoutState.Turn);
        checkLoco(loco, occ2, null, null);
        checkOcc(occupancies, [null, loco, null], "turnout turned");
    });
});

const file3 = "reverse_loop.json";

describe("Reverse loops", () => {
    it("Forward across", async () => {
        const [graph, occupancies, loco] = await setScene(file3);
        const [occ1, occ2, occ3, occ4] = occupancies;

        graph.onOccupancyChanged(occ1.pin, true);
        graph.setNext(loco, occ1);
        graph.setSpeed(loco, 10, true);
        graph.onOccupancyChanged(occ2.pin, true);
        graph.onOccupancyChanged(occ1.pin, false);
        graph.onOccupancyChanged(occ3.pin, true);
        graph.onOccupancyChanged(occ2.pin, false);

        checkLoco(loco, occ3, occ4, occ2);
        chai.expect(loco.inversion).to.equal(true);
        chai.expect(loco.speedLimit).to.equal(128);
    });
    it("Reversed across", async () => {
        const [graph, occupancies, loco] = await setScene(file3);
        const [occ1, occ2, occ3, occ4] = occupancies;
        graph.onOccupancyChanged(occ4.pin, true);
        graph.setNext(loco, occ4);
        // Yes dir is forward !
        graph.setSpeed(loco, 10, true);
        graph.onOccupancyChanged(occ3.pin, true);
        graph.onOccupancyChanged(occ4.pin, false);
        graph.onOccupancyChanged(occ2.pin, true);
        graph.onOccupancyChanged(occ3.pin, false);

        checkLoco(loco, occ2, occ1, occ3);
        chai.expect(loco.inversion).to.equal(true);
        chai.expect(loco.speedLimit).to.equal(128);
    });
});
