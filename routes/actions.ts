import * as path from "path";

import { Express, Request, Response } from "express";
import * as socket_io from "socket.io";

import { railroad_ui } from "../model/constants";
import { Crossing } from "../model/crossings";
import { Graph } from "../model/graph";
import { Hardware } from "../model/hardware";
import { Lights } from "../model/lights";
import {
    IConstants, ILocomotiveImplem, IRailRoadImplem,
} from "../model/types_implems";
import { IRailRoadUI } from "../model/types_ui";

import { ScenarioEngine } from "./scenario";
import { Signals } from "./signal";

const resdir = path.join(path.dirname(__dirname), "res");

interface SpeedBody {
    address: string;
    dir: string;
    speed: string;
}

interface AssignBody {
    address: string;
    inversion: string;
    occ: string;
}

interface LightBody {
    address: string;
    index: string;
    status: string;
}

interface BlinkBody {
    address: string;
    forward: string;
}

interface TurnoutBody {
    id: string;
    state: string;
}

interface SwitchBody {
    id: string;
    state: string;
}

interface PulseBody {
    pin: string;
    val: string;
}

interface ScenarioBody {
    scenario: string;
}

export class Actions {
    private graph: Graph;
    private hardware: Hardware;
    private railroad: IRailRoadImplem;
    private locomotives: ILocomotiveImplem[];
    private lights: Lights;
    private signals: Signals;
    private scenarios: ScenarioEngine;
    private io: socket_io.Server;
    private crossing: Crossing;

    public constructor(
        constr: IConstants, hwr: Hardware, graph: Graph, lights: Lights,
        io: socket_io.Server,
    ) {
        this.hardware = hwr;
        this.railroad = constr.railroad;
        this.lights = lights;
        this.locomotives = constr.locomotives;
        this.graph = graph;
        this.scenarios = new ScenarioEngine(constr.railroad, this.graph);
        void this.scenarios.populate();
        this.signals = new Signals(constr.railroad, this.graph, lights);
        this.crossing = new Crossing(hwr.bus);
        this.io = io;
    }

    public register(app: Express): void {
        app.route("/").get((req, res) => this.get_tracks(req, res));
        app.route("/assign").post((req, res) => this.post_assign(req, res));
        app.route("/blink").post((req, res) => this.post_blink(req, res));
        app.route("/emergency").post((req, res) => this.post_emergency(req, res));
        app.route("/image").get((req, res) => this.get_image(req, res));
        app.route("/light").post((req, res) => this.post_light(req, res));
        app.route("/lights").get((req, res) => this.get_lights(req, res));
        app.route("/railroad").get((req, res) => this.get_railroad(req, res));
        app.route("/rawturn").post((req, res) => this.post_pulse(req, res));
        app.route("/reinit").get((req, res) => this.get_reinit(req, res));
        app.route("/reset").get((req, res) => this.get_reset(req, res));
        app.route("/scenario").post((req, res) => this.post_scenario(req, res));
        app.route("/speed").post((req, res) => this.post_speed(req, res));
        app.route("/switch").post((req, res) => this.post_switch(req, res));
        app.route("/switch_direct").post((req, res) => this.post_switch_direct(req, res));
        app.route("/turnout").post((req, res) => this.post_turnout(req, res));
    }

    private error(res: Response, msg: string) {
        console.log(msg);
        res.status(400).json({error: msg});
    }

    private get_tracks(req: Request, res: Response) {
        res.render(
            "tracks.hbs",
            {
                height: this.railroad.height,
                locos: this.locomotives,
                scenarios: this.scenarios.getPrograms(),
                scripts: ["dccjs.js"],
                width: this.railroad.width,
            });
    }

    private get_image(req: Request, res: Response) {
        const id = parseInt(req.query.id as string || "0", 10);
        if (isNaN(id)) {
            this.error(res, "Bad parameters for get_image");
        } else {
            const loco = this.graph.getLocomotive(id);
            const pathImage = (
                loco === undefined || loco.image === undefined
                    ? path.join(resdir, "loco.png")
                    : loco.image);
            res.sendFile(pathImage);
        }
    }

    /** get_railroad sends a json copy of the tracks, turnouts and occupancy to the front-end. */
    private get_railroad(req: Request, res: Response) {
        // Makes a copy forgetting what belongs to the internal state of dccjs
        const railroad: IRailRoadUI = railroad_ui(this.railroad);
        res.json(railroad);
    }

    /** implements a speed modification as requested by the front-end */
    private post_speed(req: Request<unknown, unknown, SpeedBody>, res: Response) {
        const address = parseInt(req.body.address, 10);
        const speed = req.body.speed;
        const dir = req.body.dir;
        if (isNaN(address) || speed === undefined || dir === undefined) {
            this.error(res, "Bad parameters for post_speed");
            return;
        }
        const intSpeed = parseInt(speed, 10);
        const boolDir = dir === "true";
        const loco = this.graph.getLocomotive(address);
        if (loco) {
            this.graph.setSpeed(loco, intSpeed, boolDir);
            res.status(200).json({});
        } else {
            res.status(400).json({error: `No such loco ${address}`});
        }
    }


    /** implements a speed modification as requested by the front-end */
    private post_assign(req: Request<unknown, unknown, AssignBody>, res: Response) {
        const address = parseInt(req.body.address, 10);
        const occIndex = parseInt(req.body.occ, 10);
        const inversion = req.body.inversion === "true";
        if (isNaN(address) || isNaN(occIndex) || req.body.inversion === undefined) {
            this.error(res, "Bad parameters for post_assign");
            return;
        }
        const loco = this.graph.getLocomotiveAndActivate(address);
        const occ = this.railroad.occupancy[occIndex];
        if (loco === undefined || occ === undefined) {
            this.error(res, "Bad parameters for post_assign");
            return;
        }
        this.graph.assign(loco, occ, inversion);
        res.status(200).json({});
    }

    private post_light(req: Request<unknown, unknown, LightBody>, res: Response) {
        const address = parseInt(req.body.address, 10);
        const status = req.body.status;
        const index = parseInt(req.body.index, 10);
        if (isNaN(address) || isNaN(index) || status === undefined) {
            this.error(res, "unrecognized parameters post_light");
        } else {
            const loco = this.graph.getLocomotive(address);
            if (loco) {
                this.graph.setFunction(loco, index, (status === "true"));
                res.status(200).json({});
            } else {
                res.status(400).json({error: `No such loco ${address}`});
            }
        }
    }

    private get_lights(req: Request, res: Response) {
        res.render("lights.hbs", {
            helpers: {
                isModFive: (x: number) => ((x % 5) === 4),
            },
            lights: this.lights.getState(),
        });
    }

    private post_blink(req: Request<unknown, unknown, BlinkBody>, res: Response) {
        const address = parseInt(req.body.address, 10);
        const forward = req.body.forward;
        if (isNaN(address) || forward === undefined) {
            this.error(res, "unrecognized parameters post_blink");
        } else {
            const loco = this.graph.getLocomotive(address);
            if (loco) {
                void this.graph.blink(loco, (forward === "true"));
                res.status(200).json({});
            } else {
                res.status(400).json({error: `No such loco ${address}`});
            }
        }
    }

    private post_emergency(req: Request, res: Response) {
        this.graph.power(false);
        res.status(200).json({});
    }

    private post_turnout(req: Request<unknown, unknown, TurnoutBody>, res: Response) {
        const id = parseInt(req.body.id, 10);
        const state = parseInt(req.body.state, 10);
        console.log(`Change to ${id} ${state}`);
        if (isNaN(id) || isNaN(state) || state < 0 || state > 3) {
            this.error(res, "unrecognized parameters post_turnout");
        } else {
            this.graph.turn(id, state);
            res.status(200).json({});
        }
    }

    private post_switch(req: Request<unknown, unknown, SwitchBody>, res: Response) {
        const id = parseInt(req.body.id, 10);
        const stateString = req.body.state;
        if (isNaN(id) || (stateString !== "true" && stateString !== "false")) {
            this.error(res, "unrecognized parameters post_switch");
        } else {
            const state = stateString === "true";
            const swi = this.railroad.switches[id];
            const pin = swi.pin;
            if (state) {
                void this.lights.setLights([pin], []);
            } else {
                void this.lights.setLights([], [pin]);
            }
            swi.state = state;
            if (this.io) {
                this.io.emit("switch", { id, state });
            }
            res.status(200).json({});
        }
    }

    private post_switch_direct(req: Request<unknown, unknown, SwitchBody>, res: Response) {
        const id = parseInt(req.body.id, 10);
        const stateString = req.body.state;
        if (isNaN(id) || (stateString !== "true" && stateString !== "false")) {
            this.error(res, "unrecognized parameters post_switch_direct");
        } else {
            const state = stateString === "true";
            if (state) {
                void this.lights.setLights([id], []);
            } else {
                void this.lights.setLights([], [id]);
            }
            res.status(200).json({});
        }
    }

    private post_pulse(req: Request<unknown, unknown, PulseBody>, res: Response) {
        const id = parseInt(req.body.pin, 10);
        const val = parseInt(req.body.val, 10);
        if (isNaN(id) || isNaN(val) || id < 0 || val < 0 || val > 2400) {
            this.error(res, "bad parameters post_pulse");
        } else {
            void this.graph.setPulse(id, val);
        }
    }

    private post_scenario(req: Request<unknown, unknown, ScenarioBody>, res: Response) {
        const scenario = req.body.scenario;
        if (scenario === undefined) {
            this.error(res, "bad parameters for post_scenario");
        } else {
            try {
                this.scenarios.addScript(scenario);
                res.status(200).json({});
            } catch (e) {
                this.error(res, `problem with scenario ${scenario}: ${String(e)}`);
            }
        }
    }

    private get_reinit(_req: Request, _resp: Response) {
        void this.hardware.syncHardware();
    }

    private get_reset(_req: Request, _res: Response) {
        process.exit(0);
    }

}
