import * as fs from "fs-extra";
import * as jsep from "jsep";
import * as path from "path";

import { CommandStation } from "../model/central";
import { parse_turnout_state,  RES_FOLDER  } from "../model/constants";
import { Graph, IOccupancyCallback } from "../model/graph";
import { TurnoutState } from "../model/types";
import { ILocomotiveImplem, IOccupancyImplem, IRailRoadImplem} from "../model/types_implems";
import { INameSolver, solver } from "./namesolver";

/** Acquire from an occupancy detector */
interface IAcquire {
    command: "acquire";
    occupancy: number;
}

/** Wait until occupancy detector reached */
interface IAwait {
    command: "await";
    occupancy: number;
}

/** Speed /direction change */
interface ISpeed {
    command: "speed";
    speed: number;
    dir: boolean;
}

/** Modification of a turnout */
interface ITurnout {
    command: "turnout";
    id: number;
    state: TurnoutState;
}

interface ISleep {
    command: "sleep";
    delay: number;
}

interface ILight {
    command: "light";
    on: number[];
    off: number[];
}

interface IInvert {
    command: "invert";
}

/** Commands */
export type Command = ISpeed | ITurnout | ILight | IAcquire | IAwait | IInvert | ISleep;

export type Program = Command[];

interface IState {
    loco: ILocomotiveImplem | undefined;
    program: Program;
}

function intOfAst(expr: jsep.Expression): number {
    if (expr.type !== "Literal") {
        throw new Error("Expecting an integer literal");
    }
    const lit = expr as jsep.Literal;
    if (typeof lit.value !== "number") {
        throw new Error("Expecting an integer literal");
    }
    return lit.value;
}

function intOrStringOfAst(expr: jsep.Expression, idSolver: { [id: string]: number }): number {
    if (expr.type !== "Literal") {
        throw new Error("Expecting an integer literal");
    }
    const lit = expr as jsep.Literal;
    if (typeof lit.value === "boolean") {
        throw new Error("Expecting an integer literal");
    }
    if (typeof lit.value === "string") {
        const id = idSolver[lit.value];
        if (id === undefined) {
            throw new Error("Unknown resource " + lit.value);
        }
        return id;
    }
    return lit.value;
}

function intArrayOfAst(expr: jsep.Expression): number[] {
    if (expr.type !== "ArrayExpression") {
        throw new Error("Expecting an array");
    }
    const elements = (expr as jsep.ArrayExpression).elements;
    return elements.map(intOfAst);
}

function boolOfAst(expr: jsep.Expression): boolean {
    if (expr.type !== "Literal") {
        throw new Error("Expecting a boolean literal");
    }
    const lit = expr as jsep.Literal;
    if (typeof lit.value !== "boolean") {
        throw new Error("Expecting a boolean literal");
    }
    return lit.value;
}

function turnoutOfAst(expr: jsep.Expression): TurnoutState {
    if (expr.type !== "Literal") {
        throw new Error("Expecting a turnout state literal");
    }
    const lit = expr as jsep.Literal;
    if (typeof (lit.value) !== "string") {
        throw new Error("Expecting a turnout state literal");
    }
    return parse_turnout_state(lit.value);
}

/** Parses a program and gives back its equivalent js form */
export function parse(text: string, nameSolver: INameSolver): Program {
    const ast = jsep(text);
    const result: Program = [];
    if (ast.type !== "ArrayExpression") {
        throw new Error("a scenario is an array of instructions");
    }
    for (const inst of (ast as jsep.ArrayExpression).elements) {
        if (inst.type === "Identifier") {
            switch ((inst as jsep.Identifier).name) {
                case "invert":
                    result.push({ command: "invert" });
                    break;
                default:
                    throw new Error(
                        "Unknown 0 ary instruction: " +
                        (inst as jsep.Identifier).name);
            }
            continue;
        }
        if (inst.type !== "CallExpression") {
            throw new Error("members of scenarios are instructions (call expression)");
        }
        const call = inst as jsep.CallExpression;
        if (call.callee.type !== "Identifier") {
            throw new Error("Expect simple calls for instructions");
        }
        const name = (call.callee as jsep.Identifier).name;
        switch (name) {
            case "acquire":
                if (call.arguments.length !== 1) {
                    throw new Error("Bad arity for " + name);
                }
                result.push({
                    command: "acquire",
                    occupancy: intOrStringOfAst(call.arguments[0], nameSolver.occupancies) });
                break;
            case "await":
                if (call.arguments.length !== 1) {
                    throw new Error("Bad arity for " + name);
                }
                result.push({
                    command: "await",
                    occupancy: intOrStringOfAst(
                        call.arguments[0], nameSolver.occupancies),
                });
                break;
            case "speed":
                if (call.arguments.length !== 2) {
                    throw new Error("Bad arity for " + name);
                }
                result.push({
                    command: "speed",
                    dir: boolOfAst(call.arguments[1]),
                    speed: intOfAst(call.arguments[0]),
                });
                break;
            case "turnout":
                if (call.arguments.length !== 2) {
                    throw new Error("Bad arity for " + name);
                }
                result.push({
                    command: "turnout",
                    id: intOrStringOfAst(call.arguments[0], nameSolver.turnouts),
                    state: turnoutOfAst(call.arguments[1]) });
                break;
            case "sleep":
                if (call.arguments.length !== 1) {
                    throw new Error("Bad arity for " + name);
                }
                result.push({ command: "sleep", delay: intOfAst(call.arguments[0]) });
                break;
            case "light":
                if (call.arguments.length !== 2) {
                    throw new Error("Bad arity for " + name);
                }
                result.push({
                    command: "light",
                    off: intArrayOfAst(call.arguments[1]),
                    on: intArrayOfAst(call.arguments[0]),
                });
                break;
            default:
                throw new Error("Unknown n-ary instruction: " + name);
        }
    }
    return result;
}

/** ScenarioEngine executes scenarios. */
export class ScenarioEngine implements IOccupancyCallback {
    private railroad: IRailRoadImplem;
    private graph: Graph;
    private leaveMap: { [occ: number]: IState[] };
    private enterMap: { [occ: number]: IState[] };
    private programs: { [name: string]: Program };
    private states: IState[];
    private running: boolean;
    private solver: INameSolver;

    /** Builds a scenario engine.
     *
     * @param railroad: a railroad description
     * @param central: a dcc command station
     * @param turnouts: a turnout controller.
     */
    constructor(railroad: IRailRoadImplem, graph: Graph) {
        this.railroad = railroad;
        this.graph = graph;
        this.leaveMap = {};
        this.enterMap = {};
        this.states = [];
        this.programs = {};
        this.running = false;
        graph.addOccupancyCallback(this);
        this.solver = solver(railroad);
    }

    /** Reads the programs from the current folder. */
    public populate(): Promise<void> {
        const self = this;
        function readScenario(filename: string, buf: Buffer) {
            if (filename.endsWith(".sc")) {
                const name = filename.substring(0, filename.length - 3);
                const ast = parse(buf.toString(), self.solver);
                self.programs[name] = ast;
                console.log("scenario " + name + ": " + JSON.stringify(ast));
            }
        }
        const scenarioFolder = path.join(RES_FOLDER, "scenarios");
        return fs.readdir(scenarioFolder).then((filenames) =>
            Promise.all(filenames.map((name) =>
                fs.readFile(path.join(scenarioFolder, name)).then(
                    (buf) => readScenario(name, buf),
                ).catch((err) =>
                    console.log("Scenarios not correctly loaded: " + err)))),
        ).then((_) => { /* */});
    }

    /** Get the list of program names. */
    public getPrograms(): string[] {
        const result: string [] = [];
        for (const name of Object.keys(this.programs)) {
            result.push(name);
        }
        return result;
    }

    /** Creates a state from a script and launch it.
     *
     * More precisely the state is added to the list
     * of current states.
     */
    public addScript(name: string) {
        const p = this.programs[name];
        if (p === undefined) {
            throw new Error("unknown scenario: " + name);
        }
        const state: IState = { program: p.slice(), loco: undefined };
        this.addState(state);
    }

    /** run launch the execution of scenarios.
     *
     * It continues as long as there are (active) actions to
     * perform. After each pass, run reschedule itself so
     * that it does not block other code blocks (cooperative
     * scheduling).
     */
    public run() {
        console.log("run");
        const self = this;
        this.running = true;
        const states = self.states;
        this.states = [];
        const kept = states.filter((s) => self.runStep(s));
        self.states = kept.concat(self.states);
        if (self.states.length > 0) {
            setImmediate(() => self.run());
        } else {
            self.running = false;
        }
    }

    public onOccupancyChanged(occ: IOccupancyImplem) {
        const todos = occ.state ? this.enterMap[occ.id] : this.leaveMap[occ.id];
        if (todos !== undefined) {
            this.addState(todos);
        }
    }

    /** addState adds a new state to the list of states to execute.
     *
     * If the runner is not active launch it again.
     */
    private addState(state: IState | IState[]) {
        console.log("Add state " + JSON.stringify(state));
        if (Array.isArray(state)) {
            this.states = this.states.concat(state);
        } else {
            this.states.push(state);
        }
        if (!this.running) {
             console.log("Relaunching scenario engine");
             this.run();
        }
    }

    private waitForFree(occIndex: number, s: IState) {
        let map = this.leaveMap[occIndex];
        if (map === undefined) {
            map = [];
            this.leaveMap[occIndex] = map;
        }
        map.push(s);
    }

    private waitForOccupied(occIndex: number, s: IState) {
        let map = this.enterMap[occIndex];
        if (map === undefined) {
            map = [];
            this.enterMap[occIndex] = map;
        }
        map.push(s);
    }

    private runStep(s: IState): boolean {
        const inst = s.program.shift();
        if (inst === undefined) {
            return false;
        }
        switch (inst.command) {
            case "acquire": {
                throw new Error("Not implemented");
            }
            case "await": {
                const occIndex = inst.occupancy;
                const occ = this.railroad.occupancy[occIndex];
                if (occ.state) {
                    console.log("await state reached at " + occIndex);
                    if (s.loco !== undefined) {
                        const locodesc = s.loco;
                        this.graph.setNext(locodesc, occ);
                    }
                } else {
                    s.program.unshift(inst);
                    console.log("await nobody at " + occIndex);
                    this.waitForOccupied(occIndex, s);
                }
                break;
            }
            case "light":
                if (s.loco !== undefined) {
                    for (const i of inst.on) {
                        this.graph.setFunction(s.loco, i, true);
                    }
                    for (const i of inst.off) {
                        this.graph.setFunction(s.loco, i, false);
                    }
                }
                break;
            case "sleep":
                console.log("sleep " + inst.delay);
                const self = this;
                setTimeout(() => self.addState(s), inst.delay);
                return false;
            case "speed":
                console.log("speed " + inst.speed + " " + inst.dir);
                if (s.loco !== undefined) {
                    this.graph.setSpeed(s.loco, inst.speed, inst.dir);
                }
                break;
            case "turnout":
                console.log("speed " + inst.id + " " + inst.state);
                this.graph.turn(inst.id, inst.state);
                break;
            case "invert":
                console.log("invert");
                if (s.loco !== undefined) {
                    this.graph.invert(s.loco);
                }
        }
        return true;
    }

}
