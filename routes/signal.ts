import * as fs from "fs-extra";
import * as jsep from "jsep";
import * as yaml from "js-yaml";
import * as path from "path";

import { parse_turnout_state, RES_FOLDER, validate } from "../model/constants";
import { Graph, IOccupancyCallback, ITurnoutCallback } from "../model/graph";
import { Lights } from "../model/lights";
import { TurnoutState } from "../model/types";
import { IOccupancyImplem, IRailRoadImplem } from "../model/types_implems";

import { INameSolver, solver } from "./namesolver";

interface IAnd {
    logic: "and";
    args: ICond[];
}

interface IOr {
    logic: "or";
    args: ICond[];
}

interface INot {
    logic: "not";
    arg: ICond;
}

interface IOcc {
    logic: "occupancy";
    id: number;
}

interface ITrn {
    logic: "turnout";
    id: number;
    state: TurnoutState;
}

export type ICond = IAnd | IOr | INot | IOcc | ITrn;

export interface ISignal {
    name: string;
    on: string[];
    off: string[];
    condition: string;
}

export interface ISignalImplem {
    name: string;
    on: number[];
    off: number[];
    ast: ICond;
}

export interface ISignals {
    pinout: { [name: string]: number; };
    rules: ISignal[];
}

function checkPinout(isg: ISignals) {
    const used = new Set();
    for (const rule of isg.rules) {
        for (const n of rule.on) {
            used.add(n);
        }
        for (const n of rule.off) {
            used.add(n);
        }
    }
    Object.keys(isg.pinout).forEach((x) => used.delete(x));
    if (used.size > 0) {
        throw new Error(`Unknown pins were used for signal rules: ${[...used].join()}`);
    }
}

function parseExpr(ast: jsep.Expression, namesolver: INameSolver): ICond {
    switch (ast.type) {
        case "LogicalExpression":
            const log = ast as jsep.LogicalExpression;
            const args = [
                parseExpr(log.left, namesolver),
                parseExpr(log.right, namesolver)];
            if (log.operator === "&&") {
                return {logic: "and", args};
            } else if (log.operator === "||") {
                return {logic: "or", args};
            } else {
                throw new Error("unrecognized logical expression");
            }
        case "UnaryExpression":
            const uexpr = ast as jsep.UnaryExpression;
            if (uexpr.operator === "!") {
                return {logic: "not", arg: parseExpr(uexpr.argument, namesolver)};
            } else {
                throw new Error("unrecognized logical expression");
            }
        case "MemberExpression":
            const mexpr = ast as jsep.MemberExpression;
            const obj = mexpr.object;
            const prop = mexpr.property;
            if (obj.type !== "Identifier" || prop.type !== "Identifier") {
                throw new Error("unrecognized expr");
            } else {
                const tn = (obj as jsep.Identifier).name;
                const st = (prop as jsep.Identifier).name;
                const idT = namesolver.turnouts[tn];
                if (idT === undefined) {
                    throw new Error("Undefined turnout " + idT);
                }
                return {logic: "turnout", id: idT, state: parse_turnout_state(st)};
            }

        case "Identifier":
            const iexpr = ast as jsep.Identifier;
            const id = namesolver.occupancies[iexpr.name];
            if (id === undefined) {
                throw new Error("Undefined occupancy " + id);
            }
            return {logic: "occupancy", id };
        default:
            throw new Error("Not handled ast type " + ast.type);
    }
}

/** parses a condition with respect to a naming environment
 *
 * @param text: the text to parse as a condition
 * @param namesolver: a translation from names to ids for
 *  turnouts and occupancy
 * @returns an ast of the condition.
 */
function parse(text: string, namesolver: INameSolver): ICond {
    const ast = jsep(text);
    return parseExpr(ast, namesolver);
}

/** List all the turnouts that influence the value of the expression.
 *
 * There may be duplicates that must be removed.
 * @param expr: the condition
 * @returns a list of turnout ids
 */
function turnouts_of_expr(expr: ICond): number [] {
    switch (expr.logic) {
        case "and":
        case "or":
            return ([] as number[]).concat(...expr.args.map(turnouts_of_expr));
        case "not":
            return turnouts_of_expr(expr.arg);
        case "turnout":
            return [expr.id];
        default:
            return [];
    }
}

/** List all the occupancy sensors that influence the value of the expression.
 *
 * There may be duplicates that must be removed.
 * @param expr: the condition
 * @returns a list of occupancy ids
 */

function occupancy_of_expr(expr: ICond): number [] {
    switch (expr.logic) {
        case "and":
        case "or":
            return ([] as number[]).concat(...expr.args.map(occupancy_of_expr));
        case "not":
            return occupancy_of_expr(expr.arg);
        case "occupancy":
            return [expr.id];
        default:
            return [];
    }
}

/** Evaluates a condition with respect to the current state of the tracks
 *
 * @param expr: the condition to evaluate as an ast
 * @param railroad: the state of the tracks
 * @returns true if condition is met.
 */
function evaluate(expr: ICond, railroad: IRailRoadImplem): boolean {
    switch (expr.logic) {
        case "and":
            return expr.args.reduce((b, e) => (b && evaluate(e, railroad)), true as boolean);
        case "or":
            return expr.args.reduce((b, e) => (b || evaluate(e, railroad)), false as boolean);
        case "not":
            return ! evaluate(expr.arg, railroad);
        case "occupancy":
            return railroad.occupancy[expr.id].state;
        case "turnout":
            return (railroad.turnouts[expr.id].state === expr.state);
    }
}

export class Signals implements ITurnoutCallback, IOccupancyCallback {
    private railroad: IRailRoadImplem;
    private turnouts: { [id: number]: ISignalImplem[]};
    private occupancies: { [id: number]: ISignalImplem[]};
    private lights: Lights;
    private signals: ISignalImplem [] = [];
    private initialized: boolean = false;

    constructor(railroad: IRailRoadImplem, occupancy: Graph,  lights: Lights) {
        this.railroad = railroad;
        this.lights = lights;
        this.occupancies = {};
        this.turnouts = {};
        occupancy.addOccupancyCallback(this);
        occupancy.addTurnoutCallback(this);
        const namesolver = solver(railroad);
        fs.readFile(
            path.join(RES_FOLDER, "signals.yaml"),
            {encoding: "utf8" },
        ).then(
            (data) => yaml.load(data) as ISignals
        ).then(
            (data) => {
                validate(data, "ISignals");
                checkPinout(data);
                return data;
            },
        ).then((data: ISignals) => {
            this.signals = data.rules.map(
                (r) => this.populate(r, namesolver, data.pinout));
            /* First run that should initialize everybody. */
            this.refresh();
            this.initialized = true;
        }).catch(
            (err) => {
                console.log("Cannot proceed without signal description file: " + err);
            },
        );
    }

    /** Forces a global recomputation of signals. */
    public refresh() {
        this.runSignalExpression(this.signals);
    }

    public onTurnoutStateChange(id: number) {
        const signals = this.turnouts[id];
        this.runSignalExpression(signals);
    }

    public onOccupancyChanged(occ: IOccupancyImplem) {
        const signals = this.occupancies[occ.id];
        this.runSignalExpression(signals);
    }

    private populate(
        signal: ISignal,
        nameSolver: INameSolver,
        pinout: { [name: string]: number},
    ): ISignalImplem {
        const ast = parse(signal.condition, nameSolver);
        const implem: ISignalImplem = {
            ast,
            name: signal.name,
            off: signal.off.map((x) => pinout[x]),
            on: signal.on.map((x) => pinout[x]),
        };
        const turnouts = [...new Set(turnouts_of_expr(ast))];
        const occupancy = [...new Set(occupancy_of_expr(ast))];
        for (const id of turnouts) {
            let cell = this.turnouts[id];
            if (cell === undefined) {
                cell = [];
                this.turnouts[id] = cell;
            }
            cell.push(implem);
        }
        for (const id of occupancy) {
            let cell = this.occupancies[id];
            if (cell === undefined) {
                cell = [];
                this.occupancies[id] = cell;
            }
            cell.push(implem);
        }
        return implem;
    }

    private runSignalExpression(signals: ISignalImplem []) {
        if (signals === undefined || ! this.initialized) { return; }
        for (const signal of signals) {
            if (signal.ast !== undefined && evaluate(signal.ast, this.railroad)) {
                this.lights.setLights(signal.on, signal.off);
            }
        }
    }

}
