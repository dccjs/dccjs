import { IRailRoadImplem } from "../model/types_implems";

export interface INameSolver {
    turnouts: { [id: string]: number };
    occupancies: { [id: string]: number };
}

function dictOfNames(array: Array<{name: string}>): { [id: string]: number} {
    const len = array.length;
    const result: {[id: string]: number} = {};
    for (let i = 0; i < len; i++) {
        result[array[i].name] = i;
    }
    return result;
}

export function solver(railroad: IRailRoadImplem): INameSolver {
    return {
        occupancies: dictOfNames(railroad.occupancy),
        turnouts: dictOfNames(railroad.turnouts),
    };
}
