import { Tail } from "tail";

const tailSyslog = new Tail("/var/log/syslog");

export function register(io: SocketIO.Server) {
    tailSyslog.on("line", (line: string) => io.emit("log", {line}) );
}
