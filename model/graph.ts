import * as socket_io from "socket.io";

import { CommandStation } from "./central";
import { IOccupancyStateChanged, Occupancy } from "./occupancy";
import { Turnouts } from "./turnouts";
import { OccupancyKind, TurnoutState } from "./types";
import {
    IEdgeImplem, ILocomotiveImplem, IOccupancyImplem, IPresenceImplem,
    IRailRoadImplem, ITurnoutImplem,
} from "./types_implems";

const STOPPED = 0;
const SLOW = 30;
const FAST = 128;

const DELAY = 4000;

/** Interface for callbacks registered on occupancy sensor change */
export interface IOccupancyCallback {
    /** method called when the sensor is activated
     *
     * @param occ the occupancy detector triggered
     */
    onOccupancyChanged(occ: IOccupancyImplem): void;
}

/** Interface for callbacks registered on occupancy sensor change */
export interface IPresenceCallback {
    /** method called when the sensor is activated
     *
     * @param pres: the presence detector triggered
     */
    onPresence(pres: IPresenceImplem): void;
}

/** Interface for callbacks registered on turnout change */
export interface ITurnoutCallback {
    /** Called when a turnout change
     *
     * @param id the turnout that changed
     * @param state its new state
     */
    onTurnoutStateChange(id: number, state: TurnoutState): void;
}

/** Checks if an edge is active. IE the conditions on its turnouts are filled
 *
 * @param edge the edge to check
 * @return boolean true if edge is the one that can be taken accoridng to turnout
 */
function isActive(edge: IEdgeImplem): boolean {
    return edge.turnouts.every(
        ([tntImplem, state]) => tntImplem.state === state);
}

/** Finds the edges a locomotive may take. */
function findTargets(loco: ILocomotiveImplem): IEdgeImplem[] {
    const occ = loco.where;
    if (occ === null) {
        return [];
    }
    const forward = loco.forward !== loco.inversion;
    const edges = forward ? occ.next : occ.prev;
    return edges.filter(isActive);
}

/** Handling of the track and constraints imposed on locomotive
 *
 * This is the core of the DCCJS logic and acts as a middleware between
 * hardware and the web front-end. It tries to enforce the following rule:
 *
 * <ul>
 * <li> A locomotive should book the next section of track in the direction
 * it is  going</li>
 * <li> the next section of track is also determined by the state of
 * turnouts.</li>
 * <li> If there is no section of tracks the locomotive should reduce its speed.
 * We plan to introduce presence sensor that will force the locomotive to
 * stop when reached.</li>
 * <li> It there is another locomotive on the next section the locomotive
 * should stop</li>
 * <li>The locomotive may be able to restart by inverting its direction.<:li>
 * </ul>
 * The graph will notify the surounding world by emitting web events.
 */
export class Graph implements IOccupancyStateChanged {
    private railroad: IRailRoadImplem;
    private occupancies: Map<number, IOccupancyImplem | IPresenceImplem>;
    private command: CommandStation;
    private turnouts: Turnouts;
    private occupancyCallbacks: IOccupancyCallback[] = [];
    private presenceCallbacks: IPresenceCallback[] = [];
    private turnoutCallbacks: ITurnoutCallback[] = [];
    private io: socket_io.Server;
    private garage = new Map<number, ILocomotiveImplem>();

    /** Builds a graph instance
     *
     * @param railroad description of the railroad layout
     * @param locoList all the locomotives
     * @param command interface to the DCC command station (SPROG)
     * @param occupancy interface to occupancy sensor harware
     * @param turnouts interface to turnout harware
     * @param io socket io event server to notify web client.
     */
    public constructor(
        railroad: IRailRoadImplem,
        locoList: ILocomotiveImplem[],
        command: CommandStation,
        occupancy: Occupancy,
        turnouts: Turnouts,
        io: socket_io.Server,
    ) {
        this.railroad = railroad;
        this.turnouts = turnouts;
        this.command = command;
        this.io = io;
        this.occupancies = new Map<number, IOccupancyImplem | IPresenceImplem>();
        for (const occ of railroad.occupancy) {
            occ.state = false;
            this.occupancies.set(occ.pin, occ);
        }
        for (const pres of railroad.presence) {
            this.occupancies.set(pres.pin, pres);
        }
        for (const loco of locoList) {
            this.garage.set(loco.address, loco);
        }
        occupancy.addCallback(this);
        setInterval(() => {
            const obsolete = Date.now() - DELAY;
            for (const loco of locoList) {
                if (loco.wasTime !== 0 && loco.wasTime < obsolete && loco.was !== null) {
                    this.freeOccupancy(loco.was, loco);
                    loco.was = null;
                    loco.wasTime = 0;
                }
            }
        }, DELAY);
    }

    /** Adds a new callback for occupancy changes
     *
     * @param callback an occupancy callback
     */
    public addOccupancyCallback(callback: IOccupancyCallback): void {
        this.occupancyCallbacks.push(callback);
    }

    /** Adds a new callback for presence changes
     *
     * @param callback an occupancy callback
     */
    public addPresenceCallback(callback: IPresenceCallback): void {
        this.presenceCallbacks.push(callback);
    }

    /** Adds a new callback for turnout changes
     *
     * @param callback a turnout callback
     */
    public addTurnoutCallback(callback: ITurnoutCallback): void {
        this.turnoutCallbacks.push(callback);
    }

    /** Take action according to where a locomotive may go */
    public setNext(loco: ILocomotiveImplem, where: IOccupancyImplem): void {
        if (!where.state) {
            return;
        }
        if (where !== loco.where && where !== loco.next && loco.where !== null) {
            return;
        }
        /* Move loco to where if necessary. */
        if (where !== loco.where) {
            if (loco.was !== null && loco.was !== where) {
                this.freeOccupancy(loco.was, loco);
                loco.was = null;
            }
            if (loco.where) {
                loco.was = loco.where;
                loco.wasTime = Date.now();
            }
            console.log(`Setting ${loco.name} to ${where.name}`);
            if (loco.where === null) {
                this.book(where, loco);
            }
            loco.where = where;
        }
        if (where === loco.next && loco.edge !== null && loco.edge.reverseLoop) {
            loco.inversion = !loco.inversion;
        }
        this.recompute(loco);
    }

    /** Force a recomputation of setNext when the locomotive has reached its target
     *
     * @param where an occupancy sensor that just triggered and should hold a
     * locomotive that should have reached its target.
     */
    public setOccupancy(where: IOccupancyImplem): void {
        const loco = where.booked;
        if (loco !== null) {
            if (loco.next === where) {
                this.setNext(loco, where);
            } else if (loco.was === where) {
                // Reactivate the delay. Probably a bad contact somewhere.
                loco.wasTime = Date.now();
            }
        } else {
            console.log(`*** Unhandled occupancy ${where.name} ***`);
        }
    }

    /** Callback for the occupancy detectors. */
    public onOccupancyChanged(pin: number, state: boolean): void {
        const occ = this.occupancies.get(pin);
        if (occ === undefined) {
            return;
        }
        if (occ.typ === OccupancyKind.Block) {
            if (occ.state !== state) {
                occ.state = state;
                if (state) {
                    this.setOccupancy(occ);
                }
                for (const callback of this.occupancyCallbacks) {
                    callback.onOccupancyChanged(occ);
                }
                this.io.emit("occupancy", { id: occ.id, state });
            }
        } else {
            if (!state || occ.occ.booked === null) {
                return;
            }
            const loco = occ.occ.booked;
            if (((loco.forward !== loco.inversion)
                ? (occ.kind === "F")
                : (occ.kind === "B")) && loco.speedLimit === SLOW) {
                loco.speedLimit = 0;
            }
            for (const callback of this.presenceCallbacks) {
                callback.onPresence(occ);
            }
            this.io.emit("presence", { id: occ.id });
        }
        this.integrity(true);
    }

    /** Change the state of a turnout
     *
     * @param id the id of the turnout
     * @param state the new state
     */
    public turn(id: number, state: TurnoutState): void {
        const turnout = this.railroad.turnouts[id];
        if (turnout === undefined) {
            console.log(`Access to undefined turnout: ${id}`);
            return;
        }
        this.setTurnout(turnout, state);
        void this.turnouts.activate(turnout);
        for (const callback of this.turnoutCallbacks) {
            callback.onTurnoutStateChange(id, state);
        }
        this.io.emit("turnout", { id, state });
    }

    /** Gets a given locomotive in the garage (by address)
     *
     * @param address DCC address of the locomotive. It must be unique in the garage.
     */
    public getLocomotive(address: number): ILocomotiveImplem | undefined {
        return this.garage.get(address);
    }

    /** Gets the locomotive from the garage and adds it to the SPROG if necessary
     *
     * @param address DCC address of the locomotive
     */
    public getLocomotiveAndActivate(address: number): ILocomotiveImplem | undefined {
        const loco = this.garage.get(address);
        if (loco) {
            this.command.addLocomotive(loco);
        }
        return loco;
    }

    /** Sets the speed and direction of a locomotive
     *
     * @param loco locomotive datastructure
     * @param speed new speed (between 0 and 127)
     * @param dir direction true is forward.
     */
    public setSpeed(loco: ILocomotiveImplem, speed: number, dir: boolean | undefined): void {
        loco.speed = speed;
        if (dir !== undefined && dir !== loco.forward) {
            loco.forward = dir;
            this.recompute(loco);
        }
        this.integrity(true);
    }

    /** Sets the speed and direction of a locomotive
     *
     * @param loco locomotive datastructure
     * @param occ occupancy where the loco is
     * @param inversion inversion state
     */
    public assign(loco: ILocomotiveImplem, occ: IOccupancyImplem, inversion: boolean): void {
        if (loco.where !== null) {
            this.freeOccupancy(loco.where, loco);
        }
        if (loco.next !== null) {
            this.freeOccupancy(loco.next, loco);
        }
        if (loco.was !== null) {
            this.freeOccupancy(loco.was, loco);
            loco.was = null;
        }
        loco.inversion = inversion;
        loco.forward = !inversion;
        loco.speed = 0;
        this.setNext(loco, occ);
    }

    /** Change a function on a locomotive
     *
     * @param loco locomotive datastructure
     * @param fun function number
     * @param state on or off state of the function
     */
    public setFunction(loco: ILocomotiveImplem, fun: number, state: boolean): void {
        this.command.setFunction(loco, fun, state);
    }

    /** Change the inversion bit of a locomotive
     *
     * Both the tracks and locomotive are oriented. They may have a different
     * view of what is forward. false means no invesion.
     */

    public invert(loco: ILocomotiveImplem): void {
        loco.inversion = !loco.inversion;
    }

    /** make a locomotive blink in the natural forward or reverse direction
     *
     * @param loco: the locomotive that must turn on and off its lights.
     * @param forward: whether to respect or invert the normal direction.
     */
    public async blink(loco: ILocomotiveImplem, forward: boolean): Promise<void> {
        await this.command.blink(loco, forward);
    }

    /** Track power
     *
     * @param status boolean controlling if power is delivered to the track.
     */
    public power(status: boolean): void {
        this.command.power(status);
    }

    /** Debug turnout by direct command of the servo
     *
     * @param id turnout id
     * @param pulse servo value
     */
    public setPulse(id: number, pulse: number): Promise<void> {
        return this.turnouts.setPulse(id, pulse);
    }

    /** Modify a turnout state. */
    private setTurnout(
        turnout: ITurnoutImplem, state: TurnoutState,
    ): ILocomotiveImplem[] {
        turnout.state = state;
        // Compute set of edges
        const edges = turnout.edges;
        const result: ILocomotiveImplem[] = [];
        for (const edge of edges) {
            const fromLoco = edge.from.booked;
            if (fromLoco !== null && fromLoco.where === edge.from && fromLoco.forward !== fromLoco.inversion) {
                this.recompute(fromLoco);
                result.push(fromLoco);
            }
            const toLoco = edge.to.booked;
            if (toLoco !== null && toLoco.where === edge.to && toLoco.forward === toLoco.inversion) {
                this.recompute(toLoco);
                result.push(toLoco);
            }
        }
        for (const loco of result) {
            console.log(`Recomputed ${loco.name} for ${turnout.name}`);
        }
        this.integrity(true);
        return result;
    }

    private recomputeForward(loco: ILocomotiveImplem, edge: IEdgeImplem) {
        // This should work even with inversion.
        const nextOcc = loco.where === edge.from ? edge.to : edge.from;
        if (loco.next !== null && loco.next !== loco.where && loco.next !== nextOcc && loco.next !== loco.was) {
            this.freeOccupancy(loco.next, loco);
        }
        loco.next = nextOcc;
        console.log(`next of ${loco.name} is ${loco.next === null ? "-" : loco.next.name}`);
        // inversion of direction.
        if (nextOcc === loco.was) {
            loco.was = null;
            loco.wasTime = 0;
            if (nextOcc.state) {
                // weird case: current reversed while on both was and current.
                this.setNext(loco, nextOcc);
                return;
            }
        }
        loco.edge = edge;
        if (nextOcc.booked === loco) {
            loco.speedLimit = FAST;
        } else if (nextOcc.booked !== null) {
            loco.speedLimit = STOPPED;
            if (nextOcc.blocked.indexOf(loco) < 0) {
                nextOcc.blocked.push(loco);
            }
        } else {
            this.book(nextOcc, loco);
            loco.speedLimit = FAST;
        }
    }

    private recompute(loco: ILocomotiveImplem) {
        /* Find where to go next */
        const activeEdges = findTargets(loco);
        switch (activeEdges.length) {
        case 0:
            this.recomputeBlocked(loco);
            break;
        case 1:
            this.recomputeForward(loco, activeEdges[0]);
            break;
        default:
            const edgeList = activeEdges.map((e) => `${e.from.name} -> ${e.to.name}`).join(", ");
            console.log("several active edge: " + edgeList);
        }
    }

    /** Set the occupancy to that loco and emit signal.
     *
     * @param occ: the block
     * @param loco: the loco assigned to that block.
     */
    private book(occ: IOccupancyImplem, loco: ILocomotiveImplem | null) {
        occ.booked = loco;
        this.io.emit("book", { occ: occ.id, loco: loco === null ? -1 : loco.address });
    }

    /** Free the occupancy, triggering computation for next waiting. */
    private freeOccupancy(occ: IOccupancyImplem, loco: ILocomotiveImplem) {
        console.log(`${loco.name} releases ${occ.name}`);
        if (occ.booked === loco) {
            const nextLoco = occ.blocked.shift();
            if (nextLoco !== undefined) {
                console.log(`${nextLoco.name} released on ${occ.name}`);
                nextLoco.speedLimit = FAST;
                this.book(occ, nextLoco);
            } else {
                this.book(occ, null);
            }
        } else {
            occ.blocked = occ.blocked.filter((blockedLoco) => blockedLoco !== loco);
        }
    }

    private recomputeBlocked(loco: ILocomotiveImplem) {
        /* No open track (locked turnout or knocker ahead) */
        console.log(`${loco.name} has no next`);
        loco.edge = null;
        loco.speedLimit = SLOW;
        // next probably never was : to be proven.
        if (loco.next !== null && loco.next !== loco.where && loco.next !== loco.was) {
            this.freeOccupancy(loco.next, loco);
        }
        loco.next = null;
    }

    /** Strong integrity check
     *
     * @param exitOnFails: if true the process is stopped.
     */
    private integrity(exitOnFails: boolean) {
        let hasFailed = false;
        function failure(msg: string) {
            console.log(msg);
            hasFailed = true;
        }
        for (const occ of this.railroad.occupancy) {
            if (occ.booked !== null) {
                const loco = occ.booked;
                if (occ !== loco.where && occ !== loco.was && occ !== loco.next) {
                    failure(`occ ${occ.name} booked by ${loco.name} but not registered`);
                }
            }
            occ.blocked.forEach((loco) => {
                if (loco.next !== occ) {
                    failure(`${loco.name} in blocked of ${occ.name} but no next.`);
                }
                if (loco === occ.booked) {
                    failure(`${loco.name} blocks itself in ${occ.name}.`);
                }
            });
        }

        const obsolete = Date.now() - 2 * DELAY;
        for (const loco of this.garage.values()) {
            if (loco.next != null) {
                const occ = loco.next;
                if (occ.booked !== loco && occ.blocked.indexOf(loco) < 0) {
                    failure(`${loco.name} next expected in ${occ.name} but not found`);
                }
            }
            if (loco.where != null && loco.where.booked !== loco) {
                failure(`${loco.name} where expected in ${loco.where.name} but not found`);
            }
            if (loco.was != null && loco.was.booked !== loco) {
                failure(`${loco.name} where expected in ${loco.was.name} but not found`);
            }
            if (loco.where === null && loco.was !== null) {
                failure(`${loco.name} has was without where.`);
            }
            if (loco.where === null && loco.next !== null) {
                failure(`${loco.name} has next without where.`);
            }
            if (loco.was === null && loco.wasTime !== 0) {
                failure(`${loco.name} has a wasTime without was.`);
            }
            if (loco.was !== null && loco.wasTime === 0) {
                failure(`${loco.name} has a was without wasTime.`);
            }
            if (loco.wasTime > 0 && loco.wasTime < obsolete) {
                failure(`${loco.name} has an old wasTime`);
            }
        }
        if (hasFailed && exitOnFails) {
            for (const loco of this.garage.values()) {
                loco.speed = 0;
            }
            setTimeout(() => process.exit(1), 2000);
        }
    }
}
