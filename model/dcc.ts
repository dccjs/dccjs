const hex = [
    "0", "1", "2", "3", "4", "5", "6", "7",
    "8", "9", "A", "B", "C", "D", "E", "F",
];

/** Outputs a one byte raw DCC command through the SProg
 *
 * Adds the  error correcting code at the end.
 *
 * @param addres: address of the DCC decoder
 * @param nib0: high 4bits code to send
 * @param nib1: low 4bits code to send
 */
function oneByteDCC(address: number, nib0: number, nib1: number): string {
    if (address < 127) {
        const a0 = (address >> 4) & 0x7;
        const a1 = address & 0xf;
        return ("O " + hex[a0] + hex[a1] +
            " " + hex[nib0] + hex[nib1] +
            " " + hex[a0 ^ nib0] + hex[a1 ^ nib1] + "\r");
    } else {
        const a0 = 0xc | ((address >> 12) & 0x3);
        const a1 = (address >> 8) & 0xf;
        const a2 = (address >> 4) & 0xf;
        const a3 = address & 0xf;
        return ("O " + hex[a0] + hex[a1] + " " + hex[a2] + hex[a3] +
            " " + hex[nib0] + hex[nib1] +
            " " + hex[a0 ^ a2 ^ nib0] + hex[a1 ^ a3 ^ nib1] + "\r");
    }
}

/** Outputs a two byte raw DCC command through the SProg.
 *
 * Add the error correcting code at the end
 */
function twoByteDCC(
    address: number, nib0: number, nib1: number,
    nib2: number, nib3: number,
): string {
    if (address < 127) {
        const a0 = (address >> 4) & 0x7;
        const a1 = address & 0xf;
        return ("O " + hex[a0] + hex[a1] +
            " " + hex[nib0] + hex[nib1] + " " + hex[nib2] + hex[nib3] +
            " " + hex[a0 ^ nib0 ^ nib2] + hex[a1 ^ nib1 ^ nib3] + "\r");
    } else {
        const a0 = 0xc | ((address >> 12) & 0x3);
        const a1 = (address >> 8) & 0xf;
        const a2 = (address >> 4) & 0xf;
        const a3 = address & 0xf;
        return ("O " + hex[a0] + hex[a1] + " " + hex[a2] + hex[a3] +
            " " + hex[nib0] + hex[nib1] + " " + hex[nib2] + hex[nib3] +
            " " + hex[a0 ^ a2 ^ nib0 ^ nib2] + hex[a1 ^ a3 ^ nib1 ^ nib3] + "\r");
    }
}

/** Outputs a three byte raw DCC command through the SProg
 *
 * Adds the error correcting code at the end.
 */
function threeByteDCC(
    address: number, nib0: number, nib1: number,
    nib2: number, nib3: number,
    nib4: number, nib5: number,
): string {
    if (address < 127) {
        const a0 = (address >> 4) & 0x7;
        const a1 = address & 0xf;
        return ("O " + hex[a0] + hex[a1] +
            " " + hex[nib0] + hex[nib1] + " " + hex[nib2] + hex[nib3] +
            " " + hex[nib4] + hex[nib5] +
            " " + hex[a0 ^ nib0 ^ nib2 ^ nib4] + hex[a1 ^ nib1 ^ nib3 ^ nib5] +
            "\r");
    } else {
        const a0 = 0xc | ((address >> 12) & 0x3);
        const a1 = (address >> 8) & 0xf;
        const a2 = (address >> 4) & 0xf;
        const a3 = address & 0xf;
        return ("O " + hex[a0] + hex[a1] + " " + hex[a2] + hex[a3] +
            " " + hex[nib0] + hex[nib1] + " " + hex[nib2] + hex[nib3] +
            " " + hex[nib4] + hex[nib5] + " " +
            hex[a0 ^ a2 ^ nib0 ^ nib2 ^ nib4] +
            hex[a1 ^ a3 ^ nib1 ^ nib3 ^ nib5] + "\r");
    }
}

/** Standard speed in 28 step format.
 *
 * @param address address of the dcc decoder.
 * @param forward defines if the locomotive should go forward (true) or backward
 * @param ispeed speed value below 127
 */
export function speed(
    address: number, forward: boolean, ispeed: number,
): string {
    const rspeed = (ispeed <= 0) ? (ispeed === 0 ? 0 : 2) : ispeed + 2;
    return oneByteDCC(
        address,
        4 | (forward ? 2 : 0) | (rspeed & 1),
        (rspeed >> 1) & 0xf);
}

/** Extended decoder reset
 *
 * @param address address of the dcc decoder.
 * @param hard boolean defining if it is a hard or soft (false) reset.
 */
export function reset(address: number, hard: boolean): string {
    return oneByteDCC(address, 0, hard ? 0xf : 0xe);
}

/** First five functions
 *
 * @param address address of the dcc decoder.
 * @param fl: state of the special dcc light function (or F0)
 * @param state: State of the next four functions as an hexadecimal
 * digit where f1 is the low order bit.
 */
export function fun14(address: number, fl: boolean, state: number): string {
    return oneByteDCC(address, fl ? 9 : 8, state);
}

/** Functions 5 to 8
 *
 * @param address address of the dcc decoder.
 * @param state: State of the next four functions as an
 * hexadecimal digit where f5 is the low order bit.
 */
export function fun58(address: number, state: number): string {
    return oneByteDCC(address, 0xa, state);
}

/** Functions 9 to 12
 *
 * @param address address of the dcc decoder.
 * @param state: State of the next four functions as an
 * hexadecimal digit where f9 is the low order bit.
 */
export function fun9c(address: number, state: number): string {
    return oneByteDCC(address, 0xb, state);
}

/** Functions 13 to 20
 *
 * @param address address of the dcc decoder.
 * @param state: State of the next four functions as
 * an hexadecimal digit where f13 is the low order bit.
 */
export function fun13to20(address: number, state: number): string {
    return twoByteDCC(address, 0xd, 0xe, (state >> 4) & 0xf, state & 0xf);
}

/** Functions 21 to 28
 *
 * @param address address of the dcc decoder.
 * @param state: State of the next four functions
 * as an hexadecimal digit where f21 is the low order bit.
 */
export function fun21to28(address: number, state: number): string {
    return twoByteDCC(address, 0xd, 0xf, (state >> 4) & 0xf, state & 0xf);
}

export function setState(address: number, id: number, on: boolean): string {
    if (id > 127) {
        return threeByteDCC(
            address, 0xD, 0xD,
            (on ? 8 : 0) | ((id >> 12) & 0x7),
            (id >> 8) & 0xf,
            (id >> 4) & 0xf,
            id & 0xf);
    } else {
        return twoByteDCC(
            address, 0xc, 0,
            (on ? 8 : 0) | ((id >> 4) & 0x7),
            id & 0xf);
    }
}
/** Extended speed control 126 steps and long address
 *
 * Triggers an emergency stop if the speed is below 0.
 */
export function speed128(
    address: number, forward: boolean, ispeed: number,
): string {
    const rspeed = ispeed <= 0 ? (ispeed === 0 ? 0 : 1) : ispeed + 1;
    return twoByteDCC(
        address, 3, 0xf,
        (forward ? 0x8 : 0) | ((rspeed >> 4) & 0x7),
        rspeed & 0xf);
}

/** Speed restriction with enable bit */
export function restrict(address: number, ispeed?: number): string {
    if (ispeed === undefined) {
        return twoByteDCC(address, 3, 0xe, 0x8, 0);
    } else {
        return twoByteDCC(address, 3, 0xe, (ispeed >> 4) & 0x1, ispeed & 0xf);
    }
}

/** Sound control */
export function sound(address: number, volume: number): string {
    return threeByteDCC(
        address, 3, 0xd, 0, 1, (volume >> 4) & 0xf, volume & 0xf);
}

/** Sends a status query to sprog */
export function status(): string {
    return "S\r";
}

/** Sends a mode query to sprog */
export function mode(): string {
    return "M\r";
}

/** Set mode function for sprog
 *
 * @param options: the options to set as a list of strings
 *
 * * SPxx: address mode xx steps (where xx is either 14,28 or 128)
 * * R: reverse track
 * * CE: compute errors
 * * LA: long addresses
 */
export function setMode(options?: string[]): string {
    if (! options) {
        options = ["SP128"];
    }
    let addr = 2048;
    if (options.indexOf("SP14") > 0) {
        addr = 512;
    } else if (options.indexOf("SP28") > 0) {
        addr = 1024;
    }
    const modVal = (
        (options.indexOf("R") > 0 ? 256 : 0)
        | (options.indexOf("LA") > 0 ? 4096 : 0)
        | (options.indexOf("CE") > 0 ? 8 : 0) | addr);
    return `M ${modVal}\r`;
}

/** Sends a power-on command to sprog
 *
 * @param on: true set to on mode, false disable
 */
export function power(on: boolean): string {
    return (on ? "+\r" : "-\r");
}

/**
 * Select a locomotive for short commands (weird mode that should not be used)
 *
 * @param n locomotive to select
 */
export function select(n: number): string {
    return (`A${String(n)}\r`);
}
