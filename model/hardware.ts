import { Bus, BusError, BusInterface } from "async-i2c-bus";
import { networkInterfaces } from "os";
import * as  SerialPort from "serialport";

import { LCD } from "./lcddriver";
import { Lock } from "./promise_utils";
import { IHardware } from "./types";

/** Find the network addresses of the Raspberry Pi
 *  @return a list of IP v4 addresses.
 */
function interfaces(): string[] {
    const ifaces = networkInterfaces();
    const result: string[] = [];
    for (const itfs of Object.values(ifaces)) {
        if (itfs === undefined) {
            continue;
        }
        itfs.forEach(
            (def: any) => {
                if (!def.internal && def.family === "IPv4") {
                    result.push(def.address);
                }
            });
    }
    return result;
}

/** Open the serial port if necessary. Adds a logger */
function checkSerial(constants: IHardware, reset: () => void): Promise<SerialPort> {
    return new Promise<SerialPort>((resolve, reject) => {
        const baudRate = constants.baudrate === undefined ? 9600 : constants.baudrate;
        const serialport = new SerialPort(
            constants.serial,
            { baudRate, parity: "none", dataBits: 8, stopBits: 1, autoOpen: false });
        function logger(s: string) {
            const txt = s.toString();
            if (txt.substring(0, 2) === "P>") { return; }
            console.log(txt);
            if (txt.indexOf("!E3") >= 0) {
                console.log("Restarting connection");
                serialport.close((err) => { reset(); });
            }
        }
        serialport.open((err) => {
            if (err) {
                throw new Error(`Cannot open serial port ${constants.serial}: ${err}`);
            } else {
                serialport.on("data", logger);
                resolve(serialport);
            }
        });
    });
}

function checkArduino(constants: IHardware) {
    if (constants.arduino) {
        const arduinoPort = new SerialPort(
            constants.arduino,
            { baudRate: 9600, parity: "none", dataBits: 8, stopBits: 1, autoOpen: false });
        arduinoPort.open((err) => {
            if (err) {
                console.log("Cannot open arduino port: " + err);
            } else {
                arduinoPort.on("data", (s) => { console.log("Ard> " + s); });
            }
        });
    }
}

export class Hardware {
    public bus: BusInterface;
    public constants: IHardware;
    public i2cLock: Lock;
    private serialPromise: Promise<SerialPort>;

    constructor(constants: IHardware) {
        const bus = Bus();
        this.bus =  bus;

        this.constants = constants;
        this.i2cLock = new Lock();
        this.serialPromise = checkSerial(this.constants, () => this.syncSerial());
        this.init();
    }

    public async init() {
        await this.bus.open();
        const availables = await this.bus.scan();
        const errors: Array<[string, number]> = [];
        function check(addr: number, kind: string) {
            if (availables.indexOf(addr) < 0) {
                errors.push([kind, addr]);
            }
        }
        for (const addr of this.constants.i2c.lights) {
            await check(addr, "light manager");
        }
        for (const addr of this.constants.i2c.turnouts) {
            await check(addr, "turnout manager");
        }
        for (const occ of this.constants.i2c.occupancy) {
            await check(occ.address, "occupancy sensor");
        }
        for (const int of this.constants.i2c.interupt) {
            if (int.address > 0) {
                await check(int.address, "interupt handler");
            }
        }
        const screenAddr = this.constants.i2c.screen;
        if (screenAddr > 0) {
            check(screenAddr, "LCD screen");
        }
        if (errors.length > 0) {
            throw new Error("I2C devices not found" + errors.join());
        }
    }

    /** Sync the hardware ports (Serial port and I2C) */
    public syncHardware() {
        this.init();
        this.syncSerial();
    }

    public syncSerial() {
        const self = this;
        this.serialPromise = checkSerial(self.constants, () => self.syncSerial());
    }

    /** Checks the status of I2C hardware and print it on the LCD if available
     * @return a promise of a string describing the status of the hardware.
     */
    public async diagnostic(): Promise<string> {
        const i2c = this.constants.i2c;
        const port = await this.serialPromise;
        const diag = `PW: ${port ? "Y" : "N"}`;
        if (this.constants.i2c.screen > 0) {
            const lcd = new LCD(this.bus, this.i2cLock, i2c.screen);
            await lcd.init();
            await lcd.clear();
            await lcd.display_string(diag, 1);
            const addresses = interfaces();
            const address = (
                (addresses.length > 0) ? addresses[0] : "---");
            await lcd.display_string(address, 2);
        }
        return diag;
    }

    /** Correct promisified write on serial port. */
    public async writeSerial(text: string): Promise<void> {
        const port = await this.serialPromise;
        if (port !== undefined) {
            await new Promise<void>((resolve, reject) => {
                port.write(text, (err, written) => {
                    if (err) { reject(err); } else { resolve(); }
                });
            });
        }
    }

}
