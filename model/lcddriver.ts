import { BusInterface, Device, DeviceInterface } from "async-i2c-bus";
import { delay, Lock } from "./promise_utils";

// commands
export const LCD_CLEARDISPLAY = 0x01;
export const LCD_RETURNHOME = 0x02;
export const LCD_ENTRYMODESET = 0x04;
export const LCD_DISPLAYCONTROL = 0x08;
export const LCD_CURSORSHIFT = 0x10;
export const LCD_FUNCTIONSET = 0x20;
export const LCD_SETCGRAMADDR = 0x40;
export const LCD_SETDDRAMADDR = 0x80;

// flags for display entry mode
export const LCD_ENTRYRIGHT = 0x00;
export const LCD_ENTRYSHIFTRIGHT = 0x01;
export const LCD_ENTRYLEFT = 0x02;
export const LCD_ENTRYSHIFTLEFT = 0x03;

// flags for display on/off control
export const LCD_DISPLAYON = 0x04;
export const LCD_DISPLAYOFF = 0x00;
export const LCD_CURSORON = 0x02;
export const LCD_CURSOROFF = 0x00;
export const LCD_BLINKON = 0x01;
export const LCD_BLINKOFF = 0x00;

// flags for display/cursor shift
export const LCD_DISPLAYMOVE = 0x08;
export const LCD_CURSORMOVE = 0x00;
export const LCD_MOVERIGHT = 0x04;
export const LCD_MOVELEFT = 0x00;

// flags for function set
export const LCD_8BITMODE = 0x10;
export const LCD_4BITMODE = 0x00;
export const LCD_2LINE = 0x08;
export const LCD_1LINE = 0x00;
export const LCD_5X10DOTS = 0x04;
export const LCD_5X8DOTS = 0x00;

// flags for backlight control
export const LCD_BACKLIGHT = 0x08;
export const LCD_NOBACKLIGHT = 0x00;

export const En = 0b00000100; // Enable bit
export const Rw = 0b00000010; // Read/Write bit
export const Rs = 0b00000001; // Register select bit

export class LCD {
    private device: DeviceInterface;
    private lock: Lock;
    public constructor(bus: BusInterface, lock: Lock, address: number) {
        this.device = Device({ bus, address });
        this.lock = lock;
    }

    public async init(): Promise<void> {
        try {
            await this.lock.acquire();
            await this.lcd_write(0x03);
            await this.lcd_write(0x03);
            await this.lcd_write(0x03);
            await this.lcd_write(0x02);
            await this.lcd_write(LCD_FUNCTIONSET | LCD_2LINE | LCD_5X8DOTS | LCD_4BITMODE);
            await this.lcd_write(LCD_DISPLAYCONTROL | LCD_DISPLAYON);
            await this.lcd_write(LCD_CLEARDISPLAY);
            await this.lcd_write(LCD_ENTRYMODESET | LCD_ENTRYLEFT);
            await delay(200);
        } finally {
            this.lock.release();
        }
    }

    /** Displays a string at a given line number */
    public async display_string(str: string, line: number): Promise<void> {
        try {
            await this.lock.acquire();
            await this.lcd_write((line === 1) ? 0x80 : 0xC0);
            for (const char of str.split("")) {
                await this.lcd_write(char.charCodeAt(0), Rs);
            }
        } finally {
            this.lock.release();
        }
    }

    /** clear lcd and set cursor */
    public async clear(): Promise<void> {
        try {
            await this.lock.acquire();
            await this.lcd_write(LCD_CLEARDISPLAY);
            await this.lcd_write(LCD_RETURNHOME);
        } finally {
            this.lock.release();
        }
    }

    /** Writes a single bye asynchronously on the I2C bus
     *
     * @param byte the byte to write.
     * @return a promise with no result.
     */
    private async writeByte(byte: number): Promise<void> {
        await this.device.sendByte(byte);
    }

    /** Send 4 bits
     *
     * They are sent on the high quad, the low quad is set such that a pulse on EN is made.
     *
     * @param data the data shifted to send.
     *
     */
    private async lcd_write_four_bits(data: number): Promise<void> {
        await this.writeByte((data & ~En) | LCD_BACKLIGHT);
        await delay(1);
        await this.writeByte(data | En | LCD_BACKLIGHT);
        await delay(1);
        await this.writeByte((data & ~En) | LCD_BACKLIGHT);
        await delay(1);
    }

    /** write a byte command to lcd as a serie of two quads (higher first).
     *
     * @param cmd the command to send.
     * @param the mode of the command. Used to set the RS bit (register select)
     */
    private async lcd_write(cmd: number, mode?: number) {
        const m = mode === undefined ? 0 : mode;
        await this.lcd_write_four_bits(m | (cmd & 0xF0));
        await this.lcd_write_four_bits(m | ((cmd << 4) & 0xF0));
    }
}
