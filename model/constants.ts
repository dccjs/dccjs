import * as path from "path";

import * as Ajv from "ajv";
import * as fs from "fs-extra";

import {
    IEdge, IHardware, ILocomotive, IOccupancy, IPoint,
    IRailRoad, ISwitch, ITurnout, OccupancyKind, TurnoutKind, TurnoutState,
} from "./types";
import {
    IConstants, IEdgeImplem, ILocomotiveImplem, IOccupancyImplem,
    IPresenceImplem, IRailRoadImplem, ISwitchImplem, ITurnoutImplem,
} from "./types_implems";
import {
    IOccupancyUI, IPresenceUI, IRailRoadUI, ISwitchUI, ITurnoutUI,
} from "./types_ui";

/** Secret for the sessions. Choose a real one. */
export const SECRET = "abcd1234ABCD";

/** The name of the folder containing resources */
export const RES_FOLDER = "/etc/dccjs";

/** Builds a turnout from user provided information */
export function turnout_implem(turnout: ITurnout): ITurnoutImplem {
    return validate_turnout_implem({
        angle: turnout.angle,
        edges: [],
        kind: parse_turnout_kind(turnout.kind),
        name: turnout.name,
        pin: turnout.pin,
        s_pwm: turnout.s_pwm,
        state: TurnoutState.Straight,
        t_pwm: turnout.t_pwm,
        x: turnout.x,
        y: turnout.y,
    });
}

/** Check that the turnout implementation makes sense (validation after being built) */
function validate_turnout_implem(turnout: ITurnoutImplem): ITurnoutImplem {
    let expected = 1;
    switch (turnout.kind) {
    case TurnoutKind.Cross:
        expected = 0;
        break;
    case TurnoutKind.ThreeWay:
        expected = 2;
        break;
    }
    if (turnout.pin.length !== expected) {
        throw new Error(
            `pin for ${turnout.name} should contain ${expected} values.`);
    }
    if (turnout.t_pwm.length !== expected) {
        throw new Error(
            `t_pwm for ${turnout.name} should contain ${expected} values.`);
    }
    if (turnout.s_pwm.length !== expected) {
        throw new Error(
            `s_pwm for ${turnout.name} should contain ${expected} values.`);
    }
    return turnout;
}

/** UI view of a turnout from internal representation */
export function turnout_ui(turnout: ITurnoutImplem): ITurnoutUI {
    return {
        angle: turnout.angle,
        kind: turnout.kind,
        name: turnout.name,
        state: turnout.state,
        x: turnout.x,
        y: turnout.y,
    };
}

export function switch_implem(sw: ISwitch): ISwitchImplem {
    return {
        name: sw.name,
        pin: sw.pin,
        state: false,
        x: sw.x,
        y: sw.y,
    };
}

/** UI view of a switch from internal representation */
export function switch_ui(sw: ISwitchImplem): ISwitchUI {
    return {
        name: sw.name,
        state: sw.state,
        x: sw.x,
        y: sw.y,
    };
}

/** Initialize occupancy from structure read from file.
 *
 * @param occ The occupancy as read from the file
 * @param id The position of the occupancy in the array of occupancies.
 */
export function occupancy_implem(occ: IOccupancy, id: number): IOccupancyImplem {
    return {
        blocked: [],
        booked: null,
        id,
        name: occ.name,
        next: [],
        pin: occ.pin,
        presence: occ.presence,
        prev: [],
        state: false,
        typ: OccupancyKind.Block,
        x: occ.x,
        y: occ.y,
    };
}

function presence_implem(occArray: IOccupancyImplem[]): IPresenceImplem[] {
    let c = 0;
    const result = [] as IPresenceImplem[];
    for (const occ of occArray) {
        for (const pres of occ.presence) {
            const presImplem: IPresenceImplem = {
                id: c++,
                kind: pres.kind,
                name: pres.name,
                occ,
                pin: pres.pin,
                typ: OccupancyKind.Location,
                x: pres.x,
                y: pres.y,
            };
            result.push(presImplem);
        }
    }
    return result;
}

/** UI view of an occupancy from internal representation */
export function occupancy_ui(occ: IOccupancyImplem): IOccupancyUI {
    const loco = occ.booked ? occ.booked.address : -1;
    return {
        id: occ.id,
        loco,
        name: occ.name,
        state: occ.state,
        x: occ.x,
        y: occ.y,
    };
}

function presence_ui(pres: IPresenceImplem): IPresenceUI {
    return {
        id: pres.id,
        name: pres.name,
        x: pres.x,
        y: pres.y,
    };
}

/** UI view of the railroad from internal representation */
export function railroad_ui(railroad: IRailRoadImplem): IRailRoadUI {
    return {
        height: railroad.height,
        occupancy: railroad.occupancy.map(occupancy_ui),
        presence: railroad.presence.map(presence_ui),
        switches: railroad.switches.map(switch_ui),
        tracks: railroad.tracks,
        turnouts: railroad.turnouts.map(turnout_ui),
        width: railroad.width,
    };
}

/** Checks that a point is given the bounds of the drawn diagram given by width
 * and height fields.
 */
function check_bounds(kind: string, railroad: IRailRoad, p: IPoint) {
    if (p.x < 0 || p.x > railroad.width || p.y < 0 || p.y > railroad.height) {
        throw new Error(
            `${kind} at (${p.x}, ${p.y}) is out of the diagram ` +
            `bounds (${railroad.width}, ${railroad.height})`);
    }
}

/** Check that all point of railroad comply with the stated bounds. */
function check_railroad(railroad: IRailRoad) {
    for (const track of railroad.tracks) {
        for (const p of track) {
            check_bounds("track point", railroad, p);
        }
    }
    for (const turnout of railroad.turnouts) {
        check_bounds("turnout", railroad, turnout);
    }
    for (const occ of railroad.occupancy) {
        check_bounds("occupancy detectector", railroad, occ);
    }
    for (const swi of railroad.switches) {
        check_bounds("switch", railroad, swi);
    }
}

/** Check that all pins used are not used by multiple devices */
function check_pins(railroad: IRailRoad) {
    const occPins = new Set();
    const turnoutPins = new Set();
    const switchPins = new Set();
    for (const turnout of railroad.turnouts) {
        for (const pin of turnout.pin) {
            if (turnoutPins.has(pin)) {
                throw new Error(
                    `Turnout ${turnout.name} uses pin ${pin} that is` +
                    " already reserved by another turnout.");
            } else {
                turnoutPins.add(pin);
            }
        }
    }
    for (const occ of railroad.occupancy) {
        const pin = occ.pin;
        if (occPins.has(pin)) {
            throw new Error(
                `Occupancy ${occ.name} uses pin ${pin} that is` +
                " already reserved by another detector.");
        } else {
            occPins.add(pin);
        }
    }
    for (const sw of railroad.switches) {
        const pin = sw.pin;
        if (switchPins.has(pin)) {
            throw new Error(
                `Switch ${sw.name} uses pin ${pin} that is` +
                " already reserved by another switch.");
        } else {
            switchPins.add(pin);
        }
    }
}

/** Complete the model of railroad to have direct pointers to the next
 * occupancy.
 *
 * We use the edge list provided in the railroad description file. Edges
 * are linked to turnouts (that must be viewed as labels) and occupancy
 * detectors (graph nodes).
 */
function complete(railroad: IRailRoadImplem, edgeList: IEdge[]): void {
    const turnouts: { [id: string]: ITurnoutImplem } = {};
    const occupancies: { [id: string]: IOccupancyImplem } = {};
    for (const turnout of railroad.turnouts) {
        turnouts[turnout.name] = turnout;
    }

    for (const occ of railroad.occupancy) {
        occupancies[occ.name] = occ;
        occ.next = [];
        occ.prev = [];
    }

    function translate(edge: IEdge) {
        const fromOcc = occupancies[edge.from];
        if (fromOcc === undefined) {
            throw new Error(`Unknown occupancy ${edge.from} in edge`);
        }
        const toOcc = occupancies[edge.to];
        if (toOcc === undefined) {
            throw new Error(`Unknown occupancy ${edge.to} in edge`);
        }
        const tntSpec = edge.turnouts.map(
            ([turnoutName, state]: [string, string]) => {
                const turnout = turnouts[turnoutName];
                if (turnout === undefined) {
                    throw new Error(`Unknown turnout ${turnoutName} in edge`);
                }
                return [
                    turnout,
                    parse_turnout_state(state),
                ] as [ITurnoutImplem, TurnoutState];
            });
        const edgeImplem: IEdgeImplem = {
            from: fromOcc,
            reverseLoop: edge.reverseLoop !== undefined && edge.reverseLoop,
            to: toOcc,
            turnouts: tntSpec,
        };
        fromOcc.next.push(edgeImplem);
        if (edge.reverseLoop) {
            toOcc.next.push(edgeImplem);
        } else {
            toOcc.prev.push(edgeImplem);
        }
        for (const [tnt, _] of tntSpec) {
            tnt.edges.push(edgeImplem);
        }
    }

    for (const edge of edgeList) {
        translate(edge);
    }
}

function locomotive_implem(loco: ILocomotive, locoPath: string): ILocomotiveImplem {
    return {
        address: loco.address,
        c14: false,
        c58: false,
        c9c: false,
        count: 0,
        edge: null,
        forward: true,
        funVals: 0,
        functions: loco.functions,
        image: path.join(locoPath, "image.jpg"),
        inversion: false,
        name: loco.name,
        next: null,
        speed: 0,
        speedLimit: 127,
        was: null,
        wasTime: 0,
        where: null,
    };
}

/** Validate a JSON object against a schema read in the res folder.
 *
 * @param data: object to validate
 * @param schemaName: name of the schema stores in res without the .json suffix.
 * @return a promise of the data itself or throw an error
 */
export function validate<T>(data: T, schemaName: string): Promise<T> {
    const respath = path.join(path.dirname(__dirname), "res");
    return fs.readJSON(path.join(respath, schemaName + ".json")).then((schema) => {
        const ajv = new Ajv({ schemaId: "auto", allErrors: true });
        // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-unsafe-assignment
        const metaschema = require("ajv/lib/refs/json-schema-draft-04.json");
        ajv.addMetaSchema(metaschema);
        if (!ajv.validate(schema, data)) {
            throw new Error("JSON schema errors: " + JSON.stringify(ajv.errors, null, 2));
        }
        return data;
    });
}
/** get_railroad loads the current railroad from the resources. */
export function get_railroad(filePath: string): Promise<IRailRoadImplem> {
    return (fs.readJSON(filePath, { encoding: "utf8" }) as Promise<IRailRoad>).then(
        (raw) => validate(raw, "IRailRoad").then((data: IRailRoad) => {
            check_railroad(data);
            check_pins(data);
            const occImplem = data.occupancy.map(occupancy_implem);
            const railroadImplem = {
                height: data.height,
                occupancy: occImplem,
                presence: presence_implem(occImplem),
                switches: data.switches.map(switch_implem),
                tracks: data.tracks,
                turnouts: data.turnouts.map(turnout_implem),
                width: data.width,
            };
            complete(railroadImplem, data.graph);
            return railroadImplem;
        }),
    );
}

/** Reads a locomotive description file
 *
 * @param locoPath path to a folder containing a locomotive description.
 * @return a promise of locomotive description object.
 */
function read_locomotive(locoPath: string): Promise<ILocomotiveImplem> {
    const locoFile = path.join(locoPath, "loco.json");
    return (fs.readJSON(locoFile, { encoding: "utf8" }) as Promise<ILocomotive>).then(
        (data) => validate(data, "ILocomotive"),
    ).then(
        (loco) => locomotive_implem(loco, locoPath)
    ).catch(
        (err) => {
            throw new Error(`(${locoPath}): ${String(err)}`);
        },
    );
}

/** get_locomotive loads the known locomotives from the resources
 *
 * @return a promise of an array of locomotive descriptions.
 */
export function get_locomotives(locoFolder: string): Promise<ILocomotiveImplem[]> {
    return fs.readdir(locoFolder).then((filenames) =>
        Promise.all(
            filenames.map((fname) => read_locomotive(path.join(locoFolder, fname))))
    ).catch(
        (err) => {
            throw new Error(`Locomotive: ${String(err)}`);
        });
}

/** get_hardware returns values about the specific hardware settings (pins, i2c address, etc.)
 *
 * @return a promise of an hardware structure.
 */
export function get_hardware(hwFile: string): Promise<IHardware> {
    return (fs.readJSON(hwFile, { encoding: "utf8" }) as Promise<IHardware>).then(
        (data) => validate(data, "IHardware"));
}

/** get_constants loads all the constants.
 *
 * @return a promise of a triple describing the tracks, the locomotives and the hardware settings;
 */
export function get_constants(): Promise<IConstants> {
    const filePath = path.join(RES_FOLDER, "railroad.json");
    const locoFolder = path.join(RES_FOLDER, "locomotive");
    const hwFile = path.join(RES_FOLDER, "hardware.json");
    return Promise.all([
        get_railroad(filePath),
        get_locomotives(locoFolder),
        get_hardware(hwFile),
    ]).then(
        ([railroad, locomotives, hardware]) => ({ hardware, locomotives, railroad }),
    ).catch((err) => {
        console.log(err);
        process.exit(1);
        throw new Error();
    });
}

/** parse_turnout_state parses a string as a turnout state.
 *
 * Only the first letter is considered.
 *
 * @param text: text to parse
 * @returns the state as an enum.
 */
export function parse_turnout_state(text: string): TurnoutState {
    if (text.length < 1) {
        throw new Error("Empty turnout state " + text);
    }
    switch (text.substring(0, 1).toLowerCase()) {
    case "s":
        return TurnoutState.Straight;
    case "t":
        return TurnoutState.Turn;
    case "l":
        return TurnoutState.Left;
    case "r":
        return TurnoutState.Right;
    default:
        throw new Error("Unknown turnout state: " + text);
    }
}

export function parse_turnout_kind(text: string): TurnoutKind {
    if (text.length < 1) {
        throw new Error("Empty turnout kind: " + text);
    }
    switch (text.substring(0, 1).toLowerCase()) {
    case "l":
        return TurnoutKind.Left;
    case "r":
        return TurnoutKind.Right;
    case "t":
        return TurnoutKind.ThreeWay;
    case "c":
        return TurnoutKind.Cross;
    case "d":
        return TurnoutKind.DoubleSlip;
    default:
        throw new Error("Unknown turnout state: " + text);
    }

}
