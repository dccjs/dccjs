import { BusInterface } from "async-i2c-bus";
import { DIR_IN, EDGE_FALLING, on as on_gpio, setup as setup_gpio } from "rpi-gpio";
import { Lock } from "./promise_utils";
import { IInterupt } from "./types";

/** Internal version of the interupt description
 *
 * This is an extension of the interface for interupt description that
 * adds a lock.
 */
interface IInteruptImplem extends IInterupt {
    lock: Lock;
    mask: number;
}

/** An interupt listener
 *
 * @param id identifier for the sensor that triggered the interupt.
 *
 * This is the interfae that sensor must implement. In our case the id is the
 * position of the sensor in the array.
 */
export interface InteruptListener {
    handle: (id: number) => Promise<any>;
}

// De Bruijn sequence of order 4 for {0,1}
const db24 = 0b0000100110101111000;

const perm: number[] = Array(16);
for (let i = 0; i < 16; i++) {
    perm[(db24 >> i) & 0xf] = i;
}

function lg(v: number): number {
    return perm[Math.floor(db24 / v) & 0xf];
}

/** promisification of the function setting up an IRQ
 *
 * @param chan: the channel number in Raspberry Pi numbering
 * @param dir: direction of the pin (input or output)
 * @param kind: additional parameters for IRQ.
 */
function setup_irq(chan: number, dir: string, kind: string) {
    return new Promise<void>(
        (resolve, reject) =>
            setup_gpio(
                chan, dir, kind,
                (err: any) => {
                    if (err) { reject(err); } else { resolve(); }
                }));
}

/** This class implements an interupt multiplexer.
 *
 * Interupts on pins (triggered by transition to low) are either associated to
 * a sensor or to a multiplexer, namely a PCF8574. In the first case, the
 * handler of the sensor is directly called. In the later case, we first read
 * the state of the PCF8574 to discover which lanes are low, associate a sensor
 * and call its interupt handler method.
 */
export class InteruptHandler {
    private defs: { [pin: number]: IInteruptImplem } = {};
    private i2cbus: BusInterface;
    private interupts: IInteruptImplem[];
    private handler: InteruptListener;

    constructor(
        bus: BusInterface,
        interupts: IInterupt[],
        handler: InteruptListener,
    ) {
        this.i2cbus = bus;
        this.handler = handler;
        this.interupts = interupts.map((ic) => ({
                address: ic.address,
                lock: new Lock(),
                mask: (1 << ic.occupancies.length) - 1,
                occupancies: ic.occupancies,
                pin: ic.pin,
            }));
        for (const interupt of this.interupts) {
            this.defs[interupt.pin] = interupt;
        }
    }

    public async init() {
        try {
            for (const desc of this.interupts) {
                await setup_irq(desc.pin, DIR_IN, EDGE_FALLING);
            }
            on_gpio("change", (chan: number) => this.handle(chan));
            for (const desc of this.interupts) {
                await this.handle(desc.pin);
            }
        } catch (err) {
            console.log(err);
        }
    }

    private async triggers(b: number, def: IInteruptImplem) {
        // First negate input to build workset because interupt is on low
        b = ~b & def.mask;
        while (b !== 0) {
            // Find the first relevant bit
            const bit = b & -b;
            const offset = lg(bit);
            const id = def.occupancies[offset];
            if (id !== undefined) {
                await this.handler.handle(id);
            }
            // Remove bit from the workset.
            b = b & ~bit;
        }
    }

    private async handle(pin: number) {
        const handlerSpec = this.defs[pin];
        if (handlerSpec) {
            await handlerSpec.lock.acquire();
            try {
                if (handlerSpec.address === -1) {
                    for (const id of handlerSpec.occupancies) {
                        await this.handler.handle(id);
                    }
                } else {
                    const buf = Buffer.alloc(1);
                    const sz = await this.i2cbus.i2cRead(
                        handlerSpec.address, 1, buf);
                    if (sz === 1) {
                        await this.triggers(buf.readInt8(0), handlerSpec);
                    }
                }
                handlerSpec.lock.release();
            } catch (err) {
                console.log("Error during handle interupt");
                console.log(err);
                handlerSpec.lock.release();
            }
        } else {
            console.log(`Pin ${pin} not handled by interupt`);
        }
    }

}
