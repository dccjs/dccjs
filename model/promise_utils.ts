/** A few utility functions and classes for promises */

/** Asynchronous delay */
export function delay(duration: number) {
    return new Promise<void>((resolve, reject) => {
        setTimeout(() => { resolve(); }, duration);
    });
}

/** A lock class to serialize long running asynchronous computations
 *
 * Locks are necessary to establish transactions on the I2C bus.
 */
export class Lock {
    private waitQueue: Array<() => void> = [];
    private locked = false;

    /** Takes the lock. Only one pseudo thread can acquire the lock */
    public acquire(): Promise<void> {
        return new Promise<void>((resolve: () => void, reject) => {
            if (!this.locked) {
                this.locked = true;
                resolve();
            } else {
                this.waitQueue.unshift(() => resolve());
            }
        });
    }

    /** Release the lock. One waiting thread will be scheduled */
    public release() {
        const next = this.waitQueue.pop();
        if (next) {
            setImmediate(next);
        } else {
            this.locked = false;
        }
    }
}
