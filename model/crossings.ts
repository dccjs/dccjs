import { BusInterface } from "async-i2c-bus";

/** Handles i2c controlled level-crossings */

export class Crossing {
    private address: number;
    private bus: BusInterface;

    public constructor(bus: BusInterface, address = 0x12) {
        this.bus = bus;
        this.address = address;
    }

    public initialize(
        address: number, opened: number, closed: number,
        linked: boolean,
    ): Promise <void> {
        const buf = Buffer.from([0x10 + address, opened / 8, closed / 8]);
        const buf2 = Buffer.from([0x30 + address]);
        return this.write(buf).then(() => {
            if (linked) {
                return this.write(buf2);
            }
        });
    }

    public closeGate(address: number): Promise <void> {
        return this.write(Buffer.from([0x20 + address]));
    }

    public openGate(address: number): Promise <void> {
        return this.write(Buffer.from([0x28 + address]));
    }

    private async write(buf: Buffer): Promise<void> {
        await this.bus.i2cWrite(this.address, buf.length, buf);
    }

}
