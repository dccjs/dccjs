import { BusInterface } from "async-i2c-bus";
import { Direction, MCP23017 } from "mcp23017-promise";
import { Lock } from "./promise_utils";
import { IConstants } from "./types_implems";

/** Control of lights (signals) through an I2C GPIO extender. Lights are numbered from 0 and each MCP23017
 * handles 16 lights. The ith element of the lights array in the configuration file will handle the block of
 * lights from (i - 1)* 16 through i * 16 - 1.
 */
export class Lights {
    private devices: MCP23017 [];
    private lock: Lock;
    private state: boolean[];

    /** constructor.
     *
     * It will register the different GPIO expanders registered on the bus.
     *
     * @param i2cPromise: takes a promise of I2C bus.
     */
    public constructor(constr: IConstants, bus: BusInterface, lock: Lock) {
        this.lock = lock;
        this.devices =
            constr.hardware.i2c.lights.map(
                (address) => new MCP23017(bus, { address }),
            );
        const size = constr.hardware.i2c.lights.length * 16;
        this.state = new Array<boolean>(size);
        for (let i = 0; i < size; i++) {
            this.state[i] = false;
        }
    }

    public async init(): Promise<void> {
        try {
            await this.lock.acquire();
            for (const device of this.devices) {
                await device.init();
                await device.setDirection(0xFFFF, Direction.OUT);
            }
        } finally {
            this.lock.release();
        }
    }

    /** Turns on or off sets of lights.
     *
     * @param set: an array of lights to turn on
     * @param unset: an array of lights to turn off
     */
    public async setLights(set: number[], unset: number[]): Promise<void> {
        try {
            const work = new Map<number, { set: number; unset: number }>();
            for (const lightset of set) {
                this.state[lightset] = true;
                const expander = lightset >> 4;
                const pin = lightset & 0xF;
                let slot = work.get(expander);
                if (slot === undefined) {
                    slot = { set: 1 << pin, unset: 0 };
                    work.set(expander, slot);
                } else {
                    slot.set = slot.set | (1 << pin);
                }
            }
            for (const lightunset of unset) {
                this.state[lightunset] = false;
                const expander = lightunset >> 4;
                const pin = lightunset & 0xF;
                let slot = work.get(expander);
                if (slot === undefined) {
                    slot = { set: 0, unset: 1 << pin };
                    work.set(expander, slot);
                } else {
                    slot.unset = slot.unset | (1 << pin);
                }
            }
            for (const [exp, { set: wset, unset: wunset }] of work.entries()) {
                const device = this.devices[exp];
                if (device) {
                    try {
                        await this.lock.acquire();
                        await device.write(wset, wunset);
                    } finally {
                        this.lock.release();
                    }
                }
            }
        } catch (e) {
            console.error("Problem while setting lights");
            console.error(e);
        }
    }

    public getState(): boolean[] {
        return this.state;
    }
}
