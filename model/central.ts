import dcc = require("./dcc");
import hardware = require("./hardware");
import { delay, Lock } from "./promise_utils";
import { ILocomotiveImplem } from "./types_implems";

/** The DCC SPROG controller */
export class CommandStation {
    /** Locomotives currently on tracks */
    private locos: ILocomotiveImplem[] = [];
    /** Which locomotive is beinf refreshed */
    private current = 0;
    /** Timer for the refresh */
    private timer: NodeJS.Timer | null = null;
    /** Serial lock to avoid mixing commands */
    private serialLock: Lock;

    /** Default delay between refresh.
     *
     * Used as a constant but could be modified
     */
    private delay = 250;
    /** Low level hardware */
    private hardware: hardware.Hardware;

    /** Builds an SPROG state from a list of available locomotives
     *
     * @param hwr hardware main class managing the serial output.
     */
    public constructor(hwr: hardware.Hardware) {
        this.hardware = hwr;
        this.serialLock = new Lock();
    }

    /** Starts sending commands to the SPROG */
    public start(): void {
        this.timer = setInterval(() => this.loop(), this.delay);
    }

    /** Stops sending commands to the SPROG */
    public stop(): void {
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }
    }

    /** Turns on/off the power on the track.
     *
     * @param true to turn on the power.
     */
    public power(on: boolean): void {
        void this.output(dcc.power(on));
    }

    public setMode(options?: string[]): void {
        void this.output(dcc.setMode(options));
    }

    /** Set a function
     *
     * @param i index of function
     */
    public setFunction(loco: ILocomotiveImplem, i: number, state: boolean): void {
        const bit = 1 << i;
        if (state) {
            loco.funVals |= bit;
        } else {
            loco.funVals &= ~bit;
        }
        const group = Math.floor(i / 4);
        switch (group) {
        case 0:
            loco.c14 = true;
            break;
        case 1:
            loco.c58 = true;
            break;
        case 2:
            loco.c9c = true;
            break;
        default:
        }
    }

    /** get the value of a function */
    public getFunction(loco: ILocomotiveImplem, i: number): boolean {
        const bit = 1 << i;
        return (loco.funVals & bit) !== 0;
    }

    /** Adds a locomotive on the tracks (commands will be sent to it)
     *
     * @param address The DCC address of the locomotive to add.
     */
    public addLocomotive(loco: ILocomotiveImplem): ILocomotiveImplem {
        if (loco !== undefined && this.locos.indexOf(loco) === -1) {
            this.locos.push(loco);
        }
        return loco;
    }

    /** Removes the locomotive from the tracks.
     *
     * @param loco the locomotive to remove.
     */
    public removeLocomotive(loco: ILocomotiveImplem): void {
        const pos = this.locos.indexOf(loco);
        if (pos !== -1) {
            this.locos.splice(pos, 1);
        }
    }

    /** make a locomotive blink in the natural forward or reverse direction
     *
     * @param loco: the locomotive that must turn on and off its lights.
     * @param forward: whether to respect or invert the normal direction.
     *
     * This is a special function because locomotive are not registered in the
     * loop at this point.
     */
    public async blink(loco: ILocomotiveImplem, forward: boolean): Promise<void> {
        // Set the speed/direction so that the update mechanism cooperates.
        loco.speed = 0;
        loco.inversion = false;
        loco.forward = forward;
        // force speed immediately not waiting for the update.
        await this.output(dcc.speed128(loco.address, forward, 0));
        await this.output(dcc.fun14(loco.address, true, (loco.funVals >> 1) & 0xf));
        await delay(500);
        await this.output(dcc.fun14(loco.address, false, (loco.funVals >> 1) & 0xf));
    }

    /** writing command to the SPROG through the serial port if it is available
     *
     * @param command string to send to the SPROG
     */
    private async output(command: string): Promise<void> {
        await this.serialLock.acquire();
        try {
            await this.hardware.writeSerial(command);
        } finally {
            this.serialLock.release();
        }
    }

    /** Function called periodically to refresh DCC decoder
     *
     * Every 6 iteration, a function is refreshed (each function group is
     * refreshed every eighteen iteration.) This function defines the refresh
     * policy, implementation is performed by the decoder.
     *
     * @param loco: the loco whose state is pushed to the track.
     */
    private async updateDcc(loco: ILocomotiveImplem): Promise<void> {
        const funVals = loco.funVals;
        if (loco.count % 6 === 0) {
            switch (loco.count) {
            case 0:
                await this.output(dcc.fun14(
                    loco.address,
                    (funVals & 1) !== 0,
                    (funVals >> 1) & 0xf));
            case 6:
                await this.output(dcc.fun58(loco.address, (funVals >> 5) & 0xf));
            case 12:
                await this.output(dcc.fun9c(loco.address, (funVals >> 9) & 0xf));
            }
        } else {
            if (loco.c14) {
                await this.output(
                    dcc.fun14(
                        loco.address,
                        (funVals & 1) !== 0,
                        (funVals >> 1) & 0xf));
                loco.c14 = false;
            } else if (loco.c58) {
                await this.output(dcc.fun58(loco.address, (funVals >> 5) & 0xf));
                loco.c58 = false;
            } else if (loco.c9c) {
                await this.output(dcc.fun9c(loco.address, (funVals >> 9) & 0xf));
                loco.c9c = false;
            } else {
                const newMessage =
                    `${loco.address} - min(${loco.speed}, ${loco.speedLimit})` +
                    ` @ ${loco.where ? loco.where.id : -1} -> ${loco.next ? loco.next.id : -1}` +
                    ` ${String(loco.forward)}:${String(loco.inversion)}`;
                if (newMessage !== loco.debugMsg) {
                    console.log(newMessage);
                    loco.debugMsg = newMessage;
                }
                await this.output(
                    dcc.speed128(
                        loco.address,
                        loco.forward,
                        Math.min(loco.speedLimit, loco.speed)));
            }
        }
        loco.count = (loco.count + 1) % 18;
    }

    /** The function that makes the actual DCC step.
     *
     * The current loco speed is resent for renewal/accuracy as done with
     * JMRI and one of the lights/functions are sent back to but only if they
     * were modified.
     */
    private loop() {
        const locos = this.locos;
        const len = locos.length;
        if (len !== 0) {
            this.current = (this.current + 1) % len;
            const loco = locos[this.current];
            void this.updateDcc(loco);
        }
    }
}
