import { IElement, IPoint, TurnoutKind, TurnoutState } from "./types";

/** turnout contains the full description fo the turnout. */
export interface ITurnoutUI extends IElement {
    /** Angle */
    angle: number;
    /** Type of turnout (left, right, both or cross) */
    kind: TurnoutKind;
    /** State */
    state: TurnoutState;
}

/** occupancy specify how an occupancy detector is displayed on
 * the map and its state.
 */
export interface IOccupancyUI extends IElement {
    /** State of the detector */
    state: boolean;
    /** Loco address if occupied */
    loco: number;
    /** Identifier */
    id: number;
}

/** Presence only reacts to presence events and reset itself
 * automatically. We do not need a state.
 */
// tslint:disable-next-line:no-empty-interface
export interface IPresenceUI extends IElement {
    /** Number identifying detector between front and back */
    id: number;
    /** Transient state maintained in the UI. */
    state?: boolean;
}

/** switch for permanent lights */
export interface ISwitchUI extends IElement {
    /** State of the switch */
    state: boolean;
}

export interface IRailRoadUI {
    /** Width of the layout */
    width: number;
    /** Height of the layout */
    height: number;
    /** tracks: the drawing as a set of several lines.
     * Each line is an array of points
     */
    tracks: IPoint[][];
    /** An array of turnouts */
    turnouts: ITurnoutUI[];
    /** An array of occupancy detectors.
     * Each occupancy detector may be represented by several lighted points.
     */
    occupancy: IOccupancyUI[];
    /** An array of presence detectors. */
    presence: IPresenceUI[];
    /** Permanent lights */
    switches: ISwitchUI[];
}
