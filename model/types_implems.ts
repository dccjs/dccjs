import {
    IElement, IHardware, ILocomotive, IOccupancy, IPoint, IPresence,
    ISwitch, OccupancyKind, TurnoutKind, TurnoutState,
} from "./types";

/** turnout contains the full description fo the turnout. */
export interface ITurnoutImplem extends IElement {
    /** Angle */
    angle: number;
    /** Type of turnout (left, right, triple, cross or double slip) */
    kind: TurnoutKind;
    /** There may be several pin if there are several servos (motors) as it is for Three Way turnout. */
    pin: number[];
    /** pulse width modulation for going straight one value for each servo */
    s_pwm: number[];
    /** pulse width modulation for turning. one value for each servo */
    t_pwm: number[];
    /** edges using this turnout */
    edges: IEdgeImplem [];
    /** turn or straight. */
    state: TurnoutState;
}

/** One of the point that can be reached from a track segment. */
export interface IEdgeImplem {
    /** Turnouts and their expected state to reach it. */
    turnouts: Array<[ITurnoutImplem, TurnoutState]>;
    /** Is it a reverse loop ? */
    reverseLoop: boolean;
    /** Next occupancy detector (filled by dccjs) */
    from: IOccupancyImplem;
    /** Previous Occupancy detector (filled by dccjs) */
    to: IOccupancyImplem;
}

export interface ISwitchImplem extends ISwitch {
    /** State of the swith (true = lighted) */
    state: boolean;
}

/** occupancy specify how an occupancy detector is displayed on the map and its state.  */
export interface IOccupancyImplem extends IOccupancy {
    /** kind of occupancy sensor for discrimination */
    typ: OccupancyKind.Block;
    /** state of the detector */
    state: boolean;
    /** id used with front and listeners */
    id: number;
    /** What is next ?  (order is arbitrary) */
    next: IEdgeImplem [];
    /** What is before ? */
    prev: IEdgeImplem [];
    /** Booking status of the detector (Locomotive that locked the resource) */
    booked: ILocomotiveImplem | null;
    /** Blocked locomotives waiting for booked */
    blocked: ILocomotiveImplem[];
}

export interface IPresenceImplem extends IPresence {
    /** kind of occupancy sensor for discrimination */
    typ: OccupancyKind.Location;
    /** id used with front and listeners */
    id: number;
    /** Occupancy implementation supporting the presence detector */
    occ: IOccupancyImplem;
}

/** RailRoadImplem captures the complete state of the railroad. */
export interface IRailRoadImplem {
    /** Width of the layout */
    width: number;
    /** Height of the layout */
    height: number;
    /** tracks: the drawing as a set of several lines. Each line is an array of points */
    tracks: IPoint[][];
    /** An array of turnouts */
    turnouts: ITurnoutImplem[];
    /** An array of occupancy detectors. Each occupancy detector may be represented by several lighted points. */
    occupancy: IOccupancyImplem[];
    /** An array of presence detectors. Order is not relevant as they will be
     * associated to pins.
     */
    presence: IPresenceImplem[];
    /** An array of basic switches */
    switches: ISwitchImplem[];
}

/** State of a locomotive as exposed to others. */
export interface ILocomotiveImplem extends ILocomotive {
    /** Path for image */
    image: string;
    /** Current speed */
    speed: number;
    /** Direction */
    forward: boolean;
    /** Inversion with current layout */
    inversion: boolean;
    /** Functions value */
    funVals: number;
    /** Limitation of speed */
    speedLimit: number;
    /** Occupancy where the loco is or undefined. */
    where: IOccupancyImplem | null;
    /** Edge between where and next */
    edge: IEdgeImplem | null;
    /** Occupancy where the loco is going next */
    next: IOccupancyImplem | null;
    /** Occupancy where the loco was */
    was: IOccupancyImplem | null;
    /** Time when was was set for automatic removal */
    wasTime: number;
    /** Do functions 1-4 need a renewal */
    c14: boolean;
    /** Do functions 5-8 need a renewal */
    c58: boolean;
    /** Do functions 9-12 need a renewal */
    c9c: boolean;
    /** Counter to check what to renew */
    count: number;
    /** Optional debug message */
    debugMsg?: string;
}

/** All the constants */
export interface IConstants {
    /** Track description */
    railroad: IRailRoadImplem;
    /** Hardware description */
    hardware: IHardware;
    /** Array of available locomotives */
    locomotives: ILocomotiveImplem[];
}
