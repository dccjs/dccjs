/** Manages turnouts in the layout. Turnouts are controlled by servos attached to a PCA 9685 board */

import { BusInterface } from "async-i2c-bus";
import { PCA9685 } from "pca9685-promise";

import { Lock } from "./promise_utils";
import {TurnoutState} from "./types";
import { IConstants, ITurnoutImplem } from "./types_implems";

/**
 * The representation of turnout control.
 */
export class Turnouts {
    private devices: PCA9685[];
    private turnouts: ITurnoutImplem[];
    private lock: Lock;

    /** Builds the turnouts from the description of the hardware
     *
     * It contains the bus and the array of servo controller address
     * but also the pulse length for each state of the servo and the
     * mapping betweeen turnout and servos.
     */
    public constructor(constants: IConstants, bus: BusInterface, lock: Lock) {
        this.lock = lock;
        this.turnouts = constants.railroad.turnouts;
        this.devices = constants.hardware.i2c.turnouts.map(
            (address) => new PCA9685(bus, { address, frequency: 50 }),
        );
    }

    /** Initialize the devices */
    public async init(): Promise<void> {
        try {
            await this.lock.acquire();

            for (const device of this.devices) {
                await device.init();
            }
        } finally {
            this.lock.release();
        }
        for (const turnout of this.turnouts) {
            await this.activate(turnout);
        }
    }
    /** Sets the pulse modulation of a servo identified by board and pin
     *
     * @param device the board to activate
     * @param pin the pin
     * @param pulse the length of the pulse.
     *
     * Note: the I2C bus access is protected by a lock.
     */
    public async setPulseRaw(device: PCA9685, pin: number, pulse: number): Promise<void> {
        if (device) {
            await this.lock.acquire();
            try {
                await device.set_pwm_ms(pin, pulse);
            } finally {
                this.lock.release();
            }
        }
    }

    /** Sets the pulse modulation of a servo identified by id
     *
     * @param id the global identifier of the servo (identifies the board and the pin (low four bits))
     * @param pulse the length of the pulse.
     *
     * Note: the I2C bus access is protected by a lock.
     */
    public async setPulse(id: number, pulse: number): Promise<void> {
        const board = Math.floor(id / 16);
        const pin = id & 0xf;
        const device = this.devices[board];
        await this.setPulseRaw(device, pin, pulse);
    }

    /** Change the state of a turnout
     *
     * @param turnout model that must be activated on the layout
     */
    public async activate(turnout: ITurnoutImplem): Promise<void> {
        switch (turnout.state) {
        case  TurnoutState.Straight:
            for (let j = 0; j < turnout.pin.length; j++) {
                await this.setPulse(turnout.pin[j], turnout.s_pwm[j]);
            }
            break;
        case  TurnoutState.Turn:
            await this.setPulse(turnout.pin[0], turnout.t_pwm[0]);
            break;
        case  TurnoutState.Left:
            await this.setPulse(turnout.pin[1], turnout.s_pwm[1]);
            await this.setPulse(turnout.pin[0], turnout.t_pwm[0]);
            break;
        case  TurnoutState.Right:
            await this.setPulse(turnout.pin[0], turnout.s_pwm[0]);
            await this.setPulse(turnout.pin[1], turnout.t_pwm[1]);
            break;
        default:
        }
    }

}
