/** point is the type of coordinates of a 2D point on the map of the railroad */
export interface IPoint {
    x: number;
    y: number;
}

export interface IElement extends IPoint {
    name: string;
}

/** TurnoutKind enumerates the turnout kinds. */
export enum  TurnoutKind {
    /** (L)eft turning turnout */
    Left = 1,
    /** (R)ight turning turnout */
    Right = 2,
    /** (T)hree way turnout */
    ThreeWay = 3,
    /** (C)ross */
    Cross = 4,
    /** (D)ouble slip cross */
    DoubleSlip = 5,
}

/** TurnoutState enumerates the different states of a turnout. */
export enum  TurnoutState {
    /** (S)traight: no turn. Valid for all. */
    Straight = 0,
    /** (T)urning: Turning (all except Three way turnout) */
    Turn = 1,
    /** (L)eft: Turn left (Three Way) */
    Left = 2,
    /** (R)ight: Turn right (Three Way) */
    Right = 3,
}

/** turnout contains the full description fo the turnout. */
export interface ITurnout extends IElement {
    /** Angle */
    angle: number;
    /** Type of turnout (left, right, triple, cross or double slip) */
    kind: string;
    /** There may be several pin if there are several servos (motors) as it is for Three Way turnout. */
    pin: number[];
    /** pulse width modulation for going straight one value for each servo */
    s_pwm: number[];
    /** pulse width modulation for turning. one value for each servo */
    t_pwm: number[];
}

/** occupancy specify how an occupancy detector is displayed on the map and its state.  */
export interface IOccupancy extends IElement {
    /** Pin number */
    pin: number;
    /** Presence detectors in the section of the occupancy */
    presence: IPresence [];
}

export interface IPresence extends IElement {
    /** Pin number */
    pin: number;
    /** Kind of presence (regular or blocking forward/backward movement) */
    kind: "F" | "B" | "O";
}

/** switch for permanent lights */
export interface ISwitch extends IElement {
    /** Light number */
    pin: number;
}

/** Edge in the graph of tracks */
export interface IEdge {
    /** Name of the origin occupancy detector in the direction of circulation */
    from: string;
    /** Name of target occupancy detector */
    to: string;
    /** Turnouts and their expected state (as a string) */
    turnouts: Array<[string, string]>;
    /** Whether the target is inverted. Considered as false if not present */
    reverseLoop?: boolean;
}

/** RailRoad captures the complete state of the railroad. */
export interface IRailRoad {
    /** Width of the layout */
    width: number;
    /** Height of the layout */
    height: number;
    /** tracks: the drawing as a set of several lines. Each line is an array of points */
    tracks: IPoint[][];
    /** An array of turnouts */
    turnouts: ITurnout[];
    /** An array of occupancy detectors. Each occupancy detector may be represented by several lighted points. */
    occupancy: IOccupancy[];
    /** Permanent lights */
    switches: ISwitch[];
    /** Graph described by the occupancy detectors */
    graph: IEdge[];
}

/** Description of a locomotive */
export interface ILocomotive {
    /** Name used in the interface */
    name: string;
    /** DCC address */
    address: number;
    /** DCC functions available */
    functions: number[];
}

/** Kinds of occupancy detectors */
export enum OccupancyKind {
    /** Block occupancy detect a train on a section of track */
    Block,
    /** Lock occupancy detect a train (part) at a precise location. */
    Location,
}

/** Description of an occupancy sensor on the I2C bus */
export interface IOccupancyHardware {
    address: number;
    kind: OccupancyKind;
}

/** Centralized interupt handling for occupancy sensors. There is probably
 * only one interrupt line.
 */
export interface IInterupt {
    /** The pin used for the global irq associated to the interupt handler */
    pin: number;
    /** The I2C address of the handler or -1 */
    address: number;
    /** Addresses of the occupancy sensors handled. Assumes pins of the handler
     * are assigned in increasing order.
     */
    occupancies: number[];
}
/** Settings for I2C (addresses of various components) */
// tslint:disable-next-line:interface-name
export interface I2CAddresses {
    /** Address of the screen */
    screen: number;
    /** Addresses of turnouts */
    turnouts: number[];
    /** Addresses of occupancy detectors. */
    occupancy: IOccupancyHardware[];
    /** Addresses of gpio extenders */
    lights: number[];
    /** Interupt handler and assignment to occupancy.
     *
     * Assign to an I2c address a handler that will manage the interupt
     */
    interupt: IInterupt[];
}

export interface IHardware {
    /** Static state of the i2c (addresses assigned to each I2C device) */
    i2c: I2CAddresses;
    /** Name of the serial port device (usually /dev/ttyACM0) */
    serial: string;
    /** Baudrate of the main serial port. SProg operates at 9600 but arduino nano can be used reliably at 115200 */
    baudrate?: number;
    /** Sprog options */
    sprog?: string[];
    /** Name of the serial port for the arduino detector (serial debugging) */
    arduino?: string;
}
