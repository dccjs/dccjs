import { BusInterface } from "async-i2c-bus";
import { InteruptHandler, InteruptListener } from "./interupt";
import { Lock } from "./promise_utils";
import { I2CAddresses, IOccupancyHardware, OccupancyKind } from "./types";

/** Interface for callbacks registered on sensor change */
export interface IOccupancyStateChanged {
    /** method called when the sensor is activated
     *
     * @param pin: the pin triggered
     * @param state: the new state
     */
    onOccupancyChanged(pin: number, state: boolean): void;
}

/** Handles occupancy sensors spread among multiple hardware. */
export class Occupancy implements InteruptListener {

    private callbacks: IOccupancyStateChanged [];
    private bus: BusInterface;
    private hardware: IOccupancyHardware [];
    private lock: Lock;
    private state: number [];
    private interuptHandler: InteruptHandler;

    public constructor(bus: BusInterface, lock: Lock, i2c: I2CAddresses) {
        this.bus = bus;
        this.lock = lock;
        this.hardware = i2c.occupancy;
        this.callbacks = [];
        this.state = new Array<number>(i2c.occupancy.length);
        this.state.fill(0);
        this.interuptHandler = new InteruptHandler(bus, i2c.interupt, this);
    }

    /** Initialize hardware. Must be called before using the instance */
    public async init(): Promise<void> {
        await this.interuptHandler.init();
        // Recover initial state
        for (let i = 0; i < this.hardware.length; i++) {
            await this.handle(i);
        }
    }

    /** Registers a new sensor callback */
    public addCallback(callback: IOccupancyStateChanged): void {
        this.callbacks.push(callback);
        // Initialize the callbacks.
        let mask = 1;

        for (let i = 0; i < this.state.length; i++) {
            const offset = 16 * i;
            const state = this.state[i];
            for (let bit = 0; bit < 16; bit++) {
                const pin = offset + bit;
                const status = ((state & mask) !== 0);
                callback.onOccupancyChanged(pin, status);
                mask <<= 1;
            }
        }
    }

    /** Register a notifier for changes on the occupancy detectors. */
    public async handle(pos: number): Promise<void> {
        try {
            const hw = this.hardware[pos];
            const next = await this.readDetector(this.bus, hw);
            if (hw.kind === OccupancyKind.Block) {
                setImmediate(() => this.notifyBlock(pos, next));
            } else {
                setImmediate(() => this.notifyPresence(pos, next));
            }
        } catch (e) {
            console.error(e);
        }
    }

    /** Read the state of an occupancy detector
     *
     * @param bus: the global i2c bus
     * @param address: address of the detector
     * @return a promise containing the value read on the detector (a short
     * integer where each bit codes a block occupancy (1 = occupied)
     */
    private async readDetector(bus: BusInterface, hw: IOccupancyHardware): Promise<number> {
        const buf = Buffer.alloc(2);
        await this.lock.acquire();
        try {
            const l = await bus.i2cRead(hw.address, 2, buf);
            if (l !== 2) {
                throw new Error(`Incorrect length: ${String(l)}`);
            }
        } finally {
            this.lock.release();
        }
        return buf.readInt16LE(0);
    }

    /** Notifies the front-end of a change in track occupation.
     *
     * @param pos the id of the detector
     * @param next the output of the detector
     */
    private notifyBlock(pos: number, next: number): void {
        const current = this.state[pos];
        const changes: Array<[number, boolean]> = [];
        if (current !== next) {
            const difference = next ^ current;
            let mask = 1;
            for (let bit = 0; bit < 16; bit++) {
                if ((difference & mask) !== 0) {
                    const pin = pos * 16 + bit;
                    const state = ((next & mask) !== 0);
                    changes.push([pin, state]);
                    console.log(`E ${pin} ${String(state)}`);
                }
                mask <<= 1;
            }
            this.state[pos] = next;
            for (const change of changes) {
                const pin = change[0];
                const state = change[1];
                for (const callback of this.callbacks) {
                    callback.onOccupancyChanged(pin, state);
                }
            }
        }
    }

    /** Notifies the front-end of a change in presence.
     *
     * @param pos the id of the detector
     * @param next the output of the detector
     *
     * This differs from block occupancy. Not only, only the presence
     * is notified, but pins raised on two consecutive events corespond
     * to two distinct trigering of the detector.
     */
    private notifyPresence(pos: number, next: number): void {
        const changes: number[] = [];
        let mask = 1;
        for (let bit = 0; bit < 16; bit++) {
            if ((next & mask) !== 0) {
                const pin = pos * 16 + bit;
                changes.push(pin);
                console.log(`P ${pin}`);
            }
            mask <<= 1;
        }
        this.state[pos] = next;
        for (const pin of changes) {
            for (const callback of this.callbacks) {
                callback.onOccupancyChanged(pin, true);
            }
        }
    }

}
