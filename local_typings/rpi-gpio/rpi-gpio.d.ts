declare module "rpi-gpio" {
	export const  DIR_IN: string;
	export const  DIR_OUT: string;
	export const  EDGE_NONE: string; 
	export const  EDGE_RISING: string;
	export const  EDGE_FALLING: string;
	export const  EDGE_BOTH: string;
	export const  MODE_RPI: string;
	export const  MODE_BCM: string;
	export function  setup(channel: number, direction: string, mode: string, callback: (err: any) => void): void;
	export function  read(channel: number, cb?: (err:any, val: boolean) => void): void;
	export function  write(channel: number, val: boolean, cb: (err:any) => void): void
	export function  setMode(mode: string): void;
	export function  on(event: string, cb: any ): void;
}
