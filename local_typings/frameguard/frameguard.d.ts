declare module 'frameguard' {
	import express = require('express');
	function frameguard(mode:string, url?: string): express.RequestHandler;
	export = frameguard;
}