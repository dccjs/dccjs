declare module 'helmet-csp' {
	import express = require('express');
	interface IOptions {
	   directives: {
		baseUri?: string,
		childSrc?: string [],
		connectSrc?: string [],
		defaultSrc?: string [],
		fontSrc?: string [],
		formAction?: string,
		frameAncestors?: string,
		frameSrc?: string [],
		imgSrc?: string [],
		manifestSrc?: string [],
		mediaSrc?: string [],
		objectSrc?: string [],
		pluginTypes?: string [],
		sandbox?: string [],
		scriptSrc?: string [],
		styleSrc?: string [],
		upgradeInsecureRequests?: boolean,
		reportUri?: string,
	},
	   loose?: boolean,
	   reportOnly?: boolean
	   setAllHeaders?: boolean, 
	   disableAndroid?: boolean, 
	   browserSniff?: boolean
   }
	function helmet(options: IOptions): express.RequestHandler;
	export = helmet;
}