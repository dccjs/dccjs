declare module "tail" {
    export interface Logger {
        info: (data: string) => void;
        error: (error: Error) => void;
    }
    export interface TailOptions {
        separator?: string;
        fsWatchOptions?: any;
        follow?: boolean;
        logger?: Logger;
        useWatchFile?: boolean;
        encoding?: string;
    }
    export class Tail {
        constructor(name: string, options?: TailOptions);
        on(event: 'line', listener: (line: string) => void): void;
        on(event: 'error', listener: (error: Error) => void): void;
        unwatch(): void;
        watch(): void;
    }
}


