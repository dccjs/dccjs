import * as $ from "jquery";
import "jqueryui";

export function warning(msg: string) {
    const dialog = $("#dialog-warning");
    $(".message", dialog).text(msg);
    dialog.dialog("open");
}

export function initializeDialog() {
    const dialog = $("#dialog-warning");
    dialog.dialog({
        autoOpen: false,
        open: (evt, ui) => {
            console.log("HERE");
            setTimeout((() => dialog.dialog("close")), 2000);
        },
        position: { my: "right top", at: "right-5% top+5%", of: "#tracks_cv" },
    });
}
