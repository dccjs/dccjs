import * as $ from "jquery";
import * as io from "socket.io-client";

interface ILog {
    line: string;
}

function print_log(msg: ILog) {
    const logLine = $("<div></div>").text(msg.line);
    console.log("line");
    $("#log").append(logLine);
}

$(document).ready(() => {
    const socket = io();
    console.log("launching");
    socket.on("log", print_log);
});
