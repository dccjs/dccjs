import * as $ from "jquery";
import "jqueryui";
import * as notifier from "./notify";
import * as tracks from "./tracks";

function emergency() {
    "use strict";
    const data = {};
    $.ajax(
        "/emergency",
        {
            data,
            dataType: "json",
            method: "POST",
            success: (data2, status) => null,
        });
}

/// menuHandler handles click on menu items
function menuHandler(ev: Event, ui: JQueryUI.MenuUIParams) {
    if (ui.item === undefined) {
        return;
    }
    const id = ui.item.attr("id");
    if (id !== undefined) {
        if (id[0] === "S") {
            const suffix = id.substring(1);
            const nav = $("#L" + suffix);
            $(nav).toggle();
            if ($(nav).is(":visible")) {
                $("#loco-nav").tabs({ active: 1 + parseInt(suffix, 10) });
            } else {
                if ($(nav).hasClass("ui-tabs-active")) {
                    $("#loco-nav").tabs({ active: 0 });
                }
            }
        } else {
            switch (id) {
                case "W_Logs":
                    window.open("/logs.html");
                    break;
                case "T_Reset":
                    $.ajax("/reset", {
                        error: () => tracks.initializeCanvasWithTimeout(),
                        timeout: 1000,
                    });
                    break;
                case "T_Reinit":
                    $.ajax("/reinit", {
                        success: () => notifier.warning("DCCJS Reinitialized"),
                        timeout: 1000,
                    });
                    break;
                default:
                    notifier.warning("Unknown id in menu:" + id);
            }
        }
    } else {
        console.log("undefined id in menu");
    }
}

export function initialize() {
    $("#menu").menu({ select: menuHandler, position: { at: "left bottom" } });
    $("#emergency").button().click((ev, ui) => emergency());
}
