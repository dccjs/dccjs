import * as $ from "jquery";
import "jqueryui";
import * as notifier from "./notify";

function callScenario(event: JQuery.ClickEvent) {
    const name = $(event.target).attr("data-name");
    const data = { scenario: name };
    $.ajax("/scenario", {
      data,
      dataType: "json",
      error: () => { notifier.warning("cannot launch scenario " + name); },
      method: "POST",
      success: (data2, status) => {
        console.log("success " + JSON.stringify(data2));
      },
    });
  }

export function initialize() {
    $("#SCT > .scenario").each(function(i) {
        $(this).button().click(callScenario);
    });
}
