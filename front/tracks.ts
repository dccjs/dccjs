/*jslint browser: true indent:2 */

import * as $ from "jquery";
import "jqueryui";
import * as io from "socket.io-client";

import * as locoloc from "./locoloc";

import { IPoint, TurnoutKind, TurnoutState } from "../model/types";
import { IOccupancyUI, IPresenceUI, IRailRoadUI, ISwitchUI, ITurnoutUI } from "../model/types_ui";

/** Width of tracks */
const LINE_WIDTH = 11;
/** Length of a turnout object */
const TURNOUT_LENGTH = 20;
/** Length of the curved part of a turnout */
const TURNOUT_BIAS = 14;

/** Draws the straight segments of tracks */
function drawTracks(ctxt: CanvasRenderingContext2D, tracks: IPoint[][], height: number) {
  for (const track of tracks) {
    const point0 = track[0];
    ctxt.beginPath();
    ctxt.lineWidth = LINE_WIDTH;
    ctxt.moveTo(point0.x, height - point0.y);
    for (let j = 1; j < track.length; j++) {
      const point = track[j];
      ctxt.lineTo(point.x, height - point.y);
    }
    ctxt.stroke();
  }
}

/** Draws the occupancy detector with a blue dot if occupied */
function drawOccupancy(ctxt: CanvasRenderingContext2D, occupancy: IOccupancyUI[], height: number) {
  ctxt.save();
  ctxt.font = "8px Verdana";
  ctxt.lineWidth = LINE_WIDTH / 2;
  if (!occupancy) { return; }
  for (const o of occupancy) {
    const state = o.state;
    ctxt.strokeStyle = "#000";
    ctxt.fillStyle = state ? "#00F" : "#000";
    ctxt.beginPath();
    ctxt.arc(o.x, height - o.y, LINE_WIDTH, 0, 2 * Math.PI, false);
    ctxt.fill();
    ctxt.stroke();
    ctxt.fillStyle = "#F00";
    ctxt.fillText("" + o.name, o.x + 20, height - o.y + 20);
    if (o.loco !== -1) {
      ctxt.fillStyle = "#FFF";
      ctxt.fillText("" + o.loco, o.x, height - o.y);
    }
  }
  ctxt.restore();
}

/** Draws the manual switches (typically for lights) */
function drawSwitches(ctxt: CanvasRenderingContext2D, switches: ISwitchUI[], height: number) {
  ctxt.save();
  ctxt.font = "8px Verdana";
  ctxt.lineWidth = LINE_WIDTH / 2;
  for (const swi of switches) {
    const state = swi.state;
    ctxt.strokeStyle = "#000";
    ctxt.fillStyle = state ? "#FF0" : "#000";
    ctxt.beginPath();
    ctxt.arc(swi.x, height - swi.y, LINE_WIDTH, 0, 2 * Math.PI, false);
    ctxt.fill();
    ctxt.stroke();
    ctxt.fillStyle = "#F00";
    ctxt.fillText("" + swi.name, swi.x + 20, height - swi.y + 20);
  }
  ctxt.restore();
}

/** Draw a presence detector.
 * @param ctxt: the drawing context
 * @param pres: detector description
 * @param state: mode of the detector
 * @param height: height of the drawing
 */
function drawPresence(ctxt: CanvasRenderingContext2D, pres: IPresenceUI, height: number) {
  ctxt.fillStyle = pres.state ? "#F00" : "#000";
  ctxt.fillRect(pres.x - LINE_WIDTH, height - pres.y - LINE_WIDTH,
                2 * LINE_WIDTH, 2 * LINE_WIDTH);
}

/** Draw all presence dectector in off mode
 * @param ctxt: the drawing context
 * @param presences: all the detectors
 * @param height: height of the drawing
 */
function drawPresences(ctxt: CanvasRenderingContext2D, presences: IPresenceUI[], height: number) {
  for (const presence of presences) {
    drawPresence(ctxt, presence, height);
  }
}

/** Draws a turnout
 * @param ctxt: the drawing context
 * @param turnout: turnout description
 * @param height: height of the drawing
 */
function drawTurnout(ctxt: CanvasRenderingContext2D, turnout: ITurnoutUI, height: number) {
  ctxt.save();
  ctxt.strokeStyle = "#FF0";
  ctxt.translate(turnout.x, height - turnout.y);
  ctxt.fillStyle = "#F00";
  ctxt.font = "8px Verdana";
  ctxt.fillText("" + turnout.name, 20, 20);
  ctxt.rotate(turnout.angle / 180.0 * Math.PI);
  ctxt.lineWidth = LINE_WIDTH;
  ctxt.beginPath();
  switch (turnout.state) {
    case 0:
      ctxt.strokeStyle = "#0F0";
      ctxt.moveTo(-TURNOUT_LENGTH, 0);
      ctxt.lineTo(TURNOUT_LENGTH, 0);
      break;
    case 1:
      ctxt.strokeStyle = "#FF0";
      ctxt.moveTo(-TURNOUT_LENGTH, 0);
      ctxt.lineTo(0, 0);
      switch (turnout.kind) {
        case 1:
          ctxt.lineTo(TURNOUT_BIAS, -TURNOUT_BIAS);
          break;
        case 2:
          ctxt.lineTo(TURNOUT_BIAS, TURNOUT_BIAS);
          break;
        default:
      }
      break;
    case 2:
      ctxt.strokeStyle = "#F00";
      ctxt.moveTo(-TURNOUT_LENGTH, 0);
      ctxt.lineTo(0, 0);
      ctxt.lineTo(TURNOUT_BIAS, -TURNOUT_BIAS);
      break;
    case 3:
      ctxt.strokeStyle = "#F00";
      ctxt.moveTo(-TURNOUT_LENGTH, 0);
      ctxt.lineTo(0, 0);
      ctxt.lineTo(TURNOUT_BIAS, TURNOUT_BIAS);
      break;
  }
  ctxt.stroke();
  ctxt.restore();
}

/** Draws the turnouts
 * @param ctxt: the drawing context
 * @param turnouts: all the turnout descriptions
 * @param height: height of the drawing
 */
function drawTurnouts(ctxt: CanvasRenderingContext2D, turnouts: ITurnoutUI[], height: number) {
  for (const turnout of turnouts) {
    drawTurnout(ctxt, turnout, height);
  }
}

/** Change the status of a switch */
function flipSwitch(i: number, swi: ISwitchUI) {
  $.ajax("/switch", {
    data: { id: i, state: !swi.state },
    dataType: "json",
    method: "POST",
    success: () => null,
  });
}

/** Change the status of a turnout */
function flipTurnout(i: number, turnout: ITurnoutUI) {
  const data: { id: number, state?: TurnoutState } = { id: i };
  if (turnout.kind === TurnoutKind.ThreeWay) {
    data.state = (
      turnout.state === TurnoutState.Straight
        ? TurnoutState.Left
        : (turnout.state === TurnoutState.Left ? TurnoutState.Right : TurnoutState.Straight));
  } else {
    data.state = turnout.state === TurnoutState.Turn ? TurnoutState.Straight : TurnoutState.Turn;
  }
  $.ajax("/turnout", {
    data,
    dataType: "json",
    method: "POST",
    success: () => null,
  });
}

/** Event handler: single click on the canvas
 *
 * Finds the object impacted and launch the attached handler.
 */
function handleCanvasClick(x: number, y: number, model: IRailRoadUI) {
  for (let i = 0; i < model.turnouts.length; i++) {
    const turnout = model.turnouts[i];
    const dx = x - turnout.x;
    const dy = y - turnout.y;
    if ((dx * dx) + (dy * dy) < 1000) {
      flipTurnout(i, turnout);
      return;
    }
  }
  for (let i = 0; i < model.occupancy.length; i++) {
    const occupancy = model.occupancy[i];
    const dx = x - occupancy.x;
    const dy = y - occupancy.y;
    if ((dx * dx) + (dy * dy) < 1000) {
      if (occupancy.state) {
        locoloc.assignOccupancy(i, occupancy);
      }
      return;
    }
  }
  for (let i = 0; i < model.switches.length; i++) {
    const swi = model.switches[i];
    const dx = x - swi.x;
    const dy = y - swi.y;
    if ((dx * dx) + (dy * dy) < 1000) {
      flipSwitch(i, swi);
      return;
    }
  }
}

/** Draw the complete tracks */
function draw(ctxt: CanvasRenderingContext2D, model: IRailRoadUI) {
  drawTracks(ctxt, model.tracks, model.height);
  drawTurnouts(ctxt, model.turnouts, model.height);
  drawOccupancy(ctxt, model.occupancy, model.height);
  drawSwitches(ctxt, model.switches, model.height);
  drawPresences(ctxt, model.presence, model.height);
}

/** Update the state of a turnout */
function updateTurnout(ctxt: CanvasRenderingContext2D, data: IRailRoadUI, msg: any) {
  const id = msg.id;
  const state = msg.state;
  data.turnouts[id].state = state;
  $("#log").text(id);
  draw(ctxt, data);
}

/** Update the state of an occupancy sensor */
function updateOccupancy(ctxt: CanvasRenderingContext2D, data: IRailRoadUI, msg: any) {
  const id = msg.id;
  const state = msg.state;
  data.occupancy[id].state = state;
  draw(ctxt, data);
}

/** Display the presence when activated. This is a transient */
function updatePresence(ctxt: CanvasRenderingContext2D, data: IRailRoadUI, msg: any) {
  const id = msg.id;
  const presence = data.presence[id];
  presence.state = true;
  console.log(`P ${id}`);
  draw(ctxt, data);
  setTimeout(() => { presence.state = false; draw(ctxt, data); }, 500);
}

/** Update the booking status of the sensor */
function updateBooked(ctxt: CanvasRenderingContext2D, data: IRailRoadUI, msg: any) {
  const occId = msg.occ;
  const loco = msg.loco as number;
  data.occupancy[occId].loco = loco;
  draw(ctxt, data);
}

/** Update the status of a manual switch */
function updateSwitch(ctxt: CanvasRenderingContext2D, data: IRailRoadUI, msg: any) {
  const id = msg.id;
  const state = msg.state;
  data.switches[id].state = state;
  draw(ctxt, data);
}

/** Initialize the drawing and register mouse and socket.io callbacks */
function initializeTracks(data: IRailRoadUI) {
  const canvas = $<HTMLCanvasElement>("#tracks_cv");
  const ctxt = canvas[0].getContext("2d");
  canvas.mousedown(
    (evt) => {
      const rect = canvas[0].getBoundingClientRect();
      const x = (evt.clientX - rect.left) * data.width / (rect.right - rect.left);
      const y = data.height - (evt.clientY - rect.top) * data.height / (rect.bottom - rect.top);
      handleCanvasClick(x, y, data);
      return false;
    });
  if (ctxt === null) {
    return;
  }
  draw(ctxt, data);
  const socket = io();
  socket.on("turnout", (msg: string) => { updateTurnout(ctxt, data, msg); });
  socket.on("occupancy", (msg: string) => { updateOccupancy(ctxt, data, msg); });
  socket.on("presence", (msg: string) => { updatePresence(ctxt, data, msg); });
  socket.on("switch", (msg: string) => { updateSwitch(ctxt, data, msg); });
  socket.on("book", (msg: string) => { updateBooked(ctxt, data, msg); });
}

/** Request the track information and initialize the front-end */
export function initializeCanvas() {
  "use strict";
  $.ajax("/railroad", {
    dataType: "json",
    error: (err, status, text) => null,
    method: "GET",
    success: (data: IRailRoadUI, status) => initializeTracks(data),
  });
}

/** Continuously request the track information and initialize the front-end
 *
 * Loops until track info are available. Used when the system is reset.
 */
export function initializeCanvasWithTimeout() {
  "use strict";
  $.ajax("/railroad", {
    dataType: "json",
    error: (err, status, text) => initializeCanvasWithTimeout(),
    method: "GET",
    success: (data: IRailRoadUI, status) => initializeTracks(data),
    timeout: 5000,
  });
}

export function resize() {
  const canvas = $<HTMLCanvasElement>("#tracks_cv")[0];
  const rawOffset = $("#ref_text").width();
  const offset =  (rawOffset === undefined ? 0 : rawOffset) / 2;
  const height = window.innerHeight - offset;
  const ratio = canvas.width / canvas.height;
  const width = height * ratio;
  canvas.style.width = width + "px";
  canvas.style.height = height + "px";
}
