import * as $ from "jquery";
import "jqueryui";
import * as locoloc from "./locoloc";
import * as menu from "./menu";
import * as notifier from "./notify";
import * as scenarios from "./scenarios";
import * as throttle from "./throttle";
import * as tracks from "./tracks";

$(document).ready(() => {
    "use strict";
    menu.initialize();
    tracks.resize();
    tracks.initializeCanvas();
    throttle.initializePane();
    notifier.initializeDialog();
    locoloc.initializeDialog();
    scenarios.initialize();
    window.addEventListener("load", tracks.resize, false);
    window.addEventListener("resize", tracks.resize, false);
  });
