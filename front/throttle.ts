import * as $ from "jquery";
import "jqueryui";
import * as notifier from "./notify";

export interface ILocoState {
  address: number;
  index: number;
  speed: number;
  dir: boolean;
  functions: boolean[];
  // Used by locoloc: transient state of direction.
  inversion?: boolean;
}

// Creates a state for the locomotive.
function newLocoState(index: number, address: number): ILocoState {
  return {
    address,
    dir: true,
    functions: [false, false, false],
    index,
    speed: 0,
  };
}

const states: ILocoState[] = [];

export function getLoco(i: number): ILocoState {
  return states[i];
}

function publishFunction(state: ILocoState, i: number) {
  const data = {
    address: state.address,
    index: i,
    status: state.functions[i],
  };
  $.ajax("/light", {
    data,
    dataType: "json",
    method: "POST",
    success: (dt) => {
      console.log("success " + JSON.stringify(dt));
    },
  });
}

export function setFunction(state: ILocoState, i: number, s: boolean) {
  state.functions[i] = s;
  publishFunction(state, i);
}

/** Function launched when a button is pressed that sends the order
 * to the back-end.
 */
function changeFunction(state: ILocoState, i: number) {
  return (evt: JQuery.Event) => {
    state.functions[i] = !state.functions[i];
    publishFunction(state, i);
  };
}

/** update the UI with recorded speed/dir */
export function updateUi(state: ILocoState) {
  $("#M" + state.index).val(state.speed);
  $("#SL" + state.index).slider("option", "value", state.speed);
  $("#D" + state.index).text(state.dir ? "\u25b6" : "\u25c0");
}

/** Function to inform the back-end of
 * the new speed with the current direction.
 */
export function publishSpeed(state: ILocoState) {
  "use strict";
  const speed = state.speed;
  const dir = state.dir;
  const data = { address: state.address, dir, speed };
  console.log("Publish speed for " + data.address + " " + dir + " " + speed);
  $.ajax("/speed", {
    data,
    dataType: "json",
    error: (_, status) => {
      console.log("failure " + status);
    },
    method: "POST",
    success: (data2, status) => {
      updateUi(state);
    },
  });
}

function changeSpeed(state: ILocoState, delta: number): JQuery.EventHandler<HTMLElement, null> {
  return (evt: JQuery.Event) => {
    state.speed = Math.max(0, Math.min(126, state.speed + delta));
    publishSpeed(state);
  };
}

function setSpeed(state: ILocoState, speed: number): JQuery.EventHandler<HTMLElement, null> {
  return (evt: JQuery.Event) => {
    state.speed = speed;
    publishSpeed(state);
  };
}

function speedSlider(state: ILocoState): JQueryUI.SliderEvent {
  return (evt, ui) => {
    console.log(evt);
    if (ui.value !== undefined) {
      state.speed = ui.value;
      publishSpeed(state);
    }
    return true;
  };
}

function changeDir(state: ILocoState): JQuery.EventHandler<HTMLElement, null> {
  return (evt: JQuery.Event) => {
    state.dir = !state.dir;
    publishSpeed(state);
  };
}

export function initializePane() {
  $("#loco-nav").tabs();
  $("#loco-nav > ul > li.throttle-nav").hide();
  $("#loco-nav > div.throttle-div").each(function(i) {
    const attr = this.getAttribute("data-dcc");
    if (attr === null) {
      return;
    }
    const address = parseInt(attr, 10);
    const state = newLocoState(i, address);
    $(".speed-slider", this).slider({
      max: 126,
      min: 0,
      orientation: "horizontal",
      slide: speedSlider(state),
    });
    states.push(state);
    $(".address", this).text("" + address);
    $(".stop", this).button().click(setSpeed(state, 0));
    $(".slow", this).button().click(setSpeed(state, 40));
    $(".medium", this).button().click(setSpeed(state, 80));
    $(".fast", this).button().click(setSpeed(state, 120));
    $(".speed-up", this).button().click(changeSpeed(state, 1));
    $(".speed-down", this).button().click(changeSpeed(state, -1));
    $(".dir", this).button().click(changeDir(state));
    for (let f = 0; f < 3; f++) {
      $(".f" + f, this).button().click(changeFunction(state, f));
    }
  });
}
