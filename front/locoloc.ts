import * as $ from "jquery";
import "jqueryui";
import { IOccupancyUI } from "../model/types_ui";
import * as notifier from "./notify";
import * as throttle from "./throttle";

// THe selected element.
let selectedLoco: throttle.ILocoState | undefined;
let selectedOccupancy = -1;

function blink(loco: throttle.ILocoState) {
    const data = {
        address: loco.address,
        forward: !loco.inversion,
    };
    $.ajax("/blink", {
    data,
    dataType: "json",
    method: "POST",
    success: (dt) => console.log("blink"),
    });
}

function unselectLocomotive() {
    if (selectedLoco !== undefined) {
        $("#DL" + selectedLoco.index).removeClass("selected");
        selectedLoco = undefined;
    }
}

function selectLocomotive(i: number) {
    return function(evt: JQuery.Event) {
        console.log("=> " + i);
        unselectLocomotive();
        $("#DL" + i).addClass("selected");
        const loco = throttle.getLoco(i);
        loco.inversion = false;
        blink(loco);
        selectedLoco = loco;
    };
}

function invert() {
    if (selectedLoco !== undefined) {
        selectedLoco.inversion = !selectedLoco.inversion;
        blink(selectedLoco);
    }
}

function cancel() {
    unselectLocomotive();
    $("#dialog-locoloc").dialog("close");
}

function select() {
    if (selectedLoco !== undefined) {
        selectedLoco.speed = 0;
        selectedLoco.dir = !selectedLoco.inversion;
        throttle.updateUi(selectedLoco);
        const data = {
            address: selectedLoco.address,
            inversion: selectedLoco.inversion,
            occ: selectedOccupancy,
        };
        const index = selectedLoco.index;
        $.ajax("/assign", {
            data,
            dataType: "json",
            error: () => {
                notifier.warning("Cannot assign occupancy");
            },
            method: "POST",
            success: () => {
                const nav = $("#L" + index);
                $(nav).show();
            },
        });
    }
    unselectLocomotive();
    $("#dialog-locoloc").dialog("close");
}

export function assignOccupancy(i: number, occ: IOccupancyUI) {
    selectedOccupancy = i;
    const dialog = $("#dialog-locoloc");
    dialog.dialog("open");
}

export function initializeDialog() {
    const dialogLocos = $("#dialog-locoloc");
    dialogLocos.dialog({
        autoOpen: false,
        modal: true,
        position: { my: "center center", at: "center center" },
    });
    $(".loco-id", dialogLocos).each(function(i, elt) {
        $(elt).click(selectLocomotive(i));
    });
    $("#locoloc-invert-dir").button().click(() => invert());
    $("#locoloc-cancel").button().click(() => cancel());
    $("#locoloc-select").button().click(() => select());
}
